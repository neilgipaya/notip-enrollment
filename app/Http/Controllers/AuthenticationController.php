<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthenticationController extends Controller
{
    //

    public function login(Request $request){

        $credentials = $request->only('username', 'password');

        if (Auth::attempt($credentials)){

            if (Auth::user()->can('admin-dashboard')){
                return redirect('dashboard');
            }

            return redirect('profile');

        }
        else{
            $notification = array(
                'message' => 'Invalid Username / Password',
                'alert-type' => 'error'
            );


            return redirect()->back()->with($notification);
        }

    }

    public function profile()
    {
        $user = User::find(Auth::user()->id);


        return view('Users.profile', compact('user'));
    }

    /**
     * Update profile to storage
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function profileUpdate(Request $request, $id){
        User::find($id)->update($request->all());
        $notification = array(
            'message' => 'Profile Updated Successfully',
            'alert-type' => 'info'
        );


        return redirect()->back()->with($notification);
    }

    /**
     * Change users password in storage
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function changePassword(Request $request){

        $request->validate([
            'current-password' => 'required',
            'password' => 'required',
            're-password' => 'required|same:re-password',
        ]);


        if(Hash::check($request->get('current-password'), Auth::user()->password)) {

            User::find(Auth::user()->id)->update($request->only('password'));

            $notification = array(
                'message' => 'Password Updated Successfully',
                'alert-type' => 'info'
            );
        }
        else
        {
            $notification = array(
                'message' => 'There was an error updating your password',
                'alert-type' => 'warning'
            );

        }
        Auth::logout();
        return redirect('/');
    }
}
