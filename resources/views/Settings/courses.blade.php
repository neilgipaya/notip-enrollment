@extends('layout.app')

@section('content')
    <main class="main">
        <!-- Breadcrumb -->
        <ol class="breadcrumb bc-colored bg-theme" id="breadcrumb">
            <li class="breadcrumb-item ">
                <a href="">Settings</a>
            </li>
            <li class="breadcrumb-item">
                <a href="#"> Courses </a>
            </li>
        </ol>

        <div class="container-fluid">

            <div class="animated fadeIn">
                <div class="row">

                    <div class="col-md-12">
                        <div class="card card-accent-theme">

                            <div class="card-body">
                                <div align="right" >
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#create">Create </button>
                                </div>
                                <h4 class="text-theme">Courses

                                </h4>
                                <br />
                                <table class="display table table-hover table-striped dataTable" data-plugin="datatable" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th>Course </th>
{{--                                        <th>Sub Course </th>--}}
                                        <th>Course Code</th>
                                        <th>Duration</th>
                                        <th>Expiration</th>
                                        <th>Course Price</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($data as $row)
                                        <tr>
                                            <td>{{ $row->course }}</td>
{{--                                            <td>{{ $row->sub_category }}</td>--}}
                                            <td>{{ $row->course_code}}</td>
                                            <td>{{ $row->course_duration}}</td>
                                            <td>{{ $row->course_expiration}}</td>
                                            <td>{{ $row->course_price }}</td>
                                            <td class="text-nowrap" style="margin: 0">
                                                <a class="btn btn-info btn-sm m-0" title="Add Sub Courses" href="{{ url('sub-courses' . "/". $row->id) }}">
                                                    <i class="fa fa-plus"></i>
                                                </a>

                                                <a class="btn btn-warning btn-sm m-0" title="Edit" href="#" data-toggle="modal" data-target="#edit{{ $row->id }}">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                                <a class="btn btn-danger btn-sm m-0" title="Delete" href="#" data-toggle="modal" data-target="#delete{{ $row->id }}" >
                                                    <i class="fa fa-close"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- end card-body -->
                        </div>
                        <!-- end card -->
                    </div>
                    <!-- end col -->

                </div>
                <!-- end row -->
            </div>
            <!-- end animated fadeIn -->
        </div>
        <!-- end container-fluid -->
    </main>
    <!-- end main -->


    <div id="create" class="modal fade" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title">Create Courses</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form action="/courses" method="POST" id="needs-validation" novalidate="" enctype="multipart/form-data">
                    @csrf

                    <div class="modal-body">
                        <div class="form-group">
                            <label>Course</label>
                            <input type="text" class="form-control" required name="course" />
                        </div>
                        <div class="form-group">
                            <label>Course Code</label>
                            <input type="text" class="form-control" name="course_code" required />
                        </div>
                        <div class="form-group">
                            <label>Course Duration</label>
                            <input type="number" class="form-control" name="course_duration" placeholder="Number of Days" required />
                        </div>
                        <div class="form-group">
                            <label>Course Expiration</label>
                            <input type="date" class="form-control" name="course_expiration" required />
                        </div>
                        <div class="form-group">
                            <label>Course Price</label>
                            <input type="number" class="form-control" name="course_price" required />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save Record</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    @foreach($data as $row)
        <div id="edit{{ $row->id }}" class="modal fade" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h6 class="modal-title">Edit Record</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <form action="/courses/{{ $row->id }}" method="POST" id="needs-validation" novalidate="" enctype="multipart/form-data">
                        @csrf
                        @method('PATCH')

                        <div class="modal-body">
                            <div class="form-group">
                                <label>Course</label>
                                <input type="text" class="form-control" value="{{ $row->course }}" required name="course" />
                            </div>
                            <div class="form-group">
                                <label>Course Code</label>
                                <input type="text" class="form-control" value="{{ $row->course_code }}" name="course_code" />
                            </div>
                            <div class="form-group">
                                <label>Course Duration</label>
                                <input type="number" class="form-control" value="{{ $row->course_duration }}" name="course_duration" />
                            </div>
                            <div class="form-group">
                                <label>Course Expiration</label>
                                <input type="date" class="form-control" value="{{ $row->course_expiration }}" name="course_expiration" />
                            </div>
                            <div class="form-group">
                                <label>Course Price</label>
                                <input type="number" class="form-control" name="course_price" value="{{ $row->course_price }}" />
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary" >Save changes</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    @endforeach
    @foreach($data as $row)
        <div id="delete{{ $row->id }}" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h6 class="modal-title">Remove Record</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <form action="/courses/{{ $row->id }}" method="POST" id="needs-validation" novalidate="" enctype="multipart/form-data">
                        @csrf
                        @method('DELETE')

                        <div class="modal-body">
                            <h5>Delete This Record?</h5>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-danger">Remove</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    @endforeach


@endsection

@section('script')

@endsection
