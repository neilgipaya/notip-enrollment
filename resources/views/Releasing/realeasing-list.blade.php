@extends('layout.app')

@section('content')
    @inject('instructor', 'App\Http\Controllers\InstructorController')
    @inject('courseController', 'App\Http\Controllers\CoursesController')

    <main class="main">
        <!-- Breadcrumb -->
        <ol class="breadcrumb bc-colored bg-theme" id="breadcrumb">
            <li class="breadcrumb-item ">
                <a href="">Schedule</a>
            </li>
            <li class="breadcrumb-item">
                <a href="#"> Manage Schedule </a>
            </li>
        </ol>

        <div class="container-fluid">

            <div class="animated fadeIn">
                <div class="row">

                    <div class="col-md-12">
                        <div class="card card-accent-theme">

                            <div class="card-body">
                                @can('create-schedules')
                                    <div align="right" >
                                        <a href="{{ url('schedule/create') }}" class="btn btn-primary" >Create </a>
                                    </div>
                                @endcan
                                <h4 class="text-theme">Schedule List

                                </h4>
                                <br />
                                <table class="display table table-hover table-striped dataTable" data-plugin="datatable" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th>Course</th>
                                        <th>Sub Course</th>
                                        <th>Class Number</th>
                                        <th>Schedule Date</th>
                                        <th>Duration</th>
                                        <th>Assessor </th>
                                        <th>Supervisor</th>
                                        <th>Enrolled Students</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($data as $row)
                                        <tr>
                                            <td>{{ $row->course }}</td>
                                            <td>{{ $courseController::subCourse($row->schedule_sub_course_id) }}</td>
                                            <td>{{ $row->schedule_course_id }}</td>
                                            <td>{{ $row->schedule_from . "-" . $row->schedule_to }}</td>
                                            <td>{{ $row->schedule_duration }} Days</td>
                                            <td>{{ $instructor::getAssessor($row->schedule_assessor_id) }}</td>
                                            <td>{{ $instructor::getSuperVisor($row->schedule_supervisor_id) }}</td>
                                            <td>
                                                @if($instructor::countEnrollees($row->schedule_course_id) > 0)
                                                    <span class="badge badge-info text-white">{{ $instructor::countEnrollees($row->schedule_course_id) }}</span>
                                                @else
                                                    <span class="badge badge-danger text-white">{{ $instructor::countEnrollees($row->schedule_course_id) }}</span>
                                                @endif
                                            </td>
                                            <td class="text-nowrap" style="margin: 0">
                                                <a class="btn btn-info btn-sm m-0" title="Print TCROA" data-toggle="modal" data-target="#print{{ $row->id }}">
                                                    <i class="fa fa-print"></i>
                                                </a>
{{--                                                <a class="btn btn-info btn-sm m-0" title="View Students" href="">--}}
{{--                                                    <i class="fa fa-eye"></i>--}}
{{--                                                </a>--}}
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- end card-body -->
                        </div>
                        <!-- end card -->
                    </div>
                    <!-- end col -->

                </div>
                <!-- end row -->
            </div>
            <!-- end animated fadeIn -->
        </div>
        <!-- end container-fluid -->
    </main>
    <!-- end main -->

    @foreach($data as $row)
        <div id="print{{ $row->id }}" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h6 class="modal-title">Print TCROA</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <form action="/export" method="POST" id="needs-validation" novalidate="" enctype="multipart/form-data">
                        @csrf

                        <div class="modal-body">
                            <input type="hidden" value="{{ $row->schedule_course_id }}" name="course_code" />
                            <input type="hidden" value="{{ $row->schedule_duration }}" name="schedule_duration" />
                            <div class="form-group">
                                <label>Select Format</label>
                                <select class="form-control select2" name="format" data-plugin="select2" style="width: 100%">
                                    <option value="ACTO">ACTO</option>
                                    <option value="AFF">AFF</option>
                                    <option value="ALTO">ALTO</option>
                                    <option value="AOTO">AOTO</option>
                                    <option value="ATFF">ATFF</option>
                                    <option value="BLGT">BLGT</option>
                                    <option value="BTOC">BTOC</option>
                                    <option value="BTR">BTR</option>
                                    <option value="MECA">MECA</option>
                                    <option value="MEFA">MEFA</option>
                                    <option value="PSCRB">PSCRB</option>
                                    <option value="PSCRB-R">PSCRB-R</option>
                                    <option value="SDSD">SDSD</option>
                                    <option value="SSO">SSO</option>
                                </select>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-danger">Print TCROA</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    @endforeach


@endsection

@section('script')

@endsection
