@extends('layout.app')

@section('content')
    <main class="main">
        <!-- Breadcrumb -->

        <div class="container-fluid">

            <div class="animated fadeIn">

                <div class="row mt-lg-5">
                    <div class="col-md-12">
                        <div class="card card-accent-theme">
                            <div class="card-body">
                                @can('user-create')
                                    <div align="right" >
                                        <a href="{{ url('users/create') }}" class="btn btn-primary">Create </a>
                                    </div>
                                @endcan
                                <h4 class="text-theme">
                                    System Users
                                </h4>
                                <table class="table table-hover dataTable table-striped w-full" data-plugin="dataTable" width="100%">
                                    <thead>
                                    <tr>
                                        <th>Username</th>
                                        <th>Email</th>
                                        <th>Full Name</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @foreach($data as $row)
                                        <tr>
                                            <td>{{ $row->username }}</td>
                                            <td>{{ $row->email }}</td>
                                            <td>{{ $row->name }}</td>
                                            <td>
                                                @can('user-edit')
                                                <a href="" data-target="#edit{{ $row->id }}" data-toggle="modal" class="btn
                                                        btn-xs
                                                        btn-warning" style="margin-top: -5px"title="Edit Record"><i class="icon-pencil"
                                                                                                                    style="color: black;"></i>
                                                </a>
                                                <a href="" data-target="#reset{{ $row->id }}" data-toggle="modal" class="btn
                                                    btn-xs
                                                    btn-info" style="margin-top: -5px"title="Reset Password"><i class="icon-refresh"
                                                                                                                style="color: black;"></i>
                                                </a>
                                               @endcan
                                                @can('user-delete')
                                                    <a href="" data-target="#delete{{ $row->id }}" data-toggle="modal" class="btn
                                                            btn-xs  btn-danger" style="margin-top: -5px" title="Delete Record"><i class="icon-trash"
                                                                                                                                  style="color: white;"></i>
                                                    </a>
                                                @endcan
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                            </div>
                            <!-- end card-body -->
                        </div>
                        <!-- end card -->
                    </div>
                </div>
            </div>
            <!-- end animated fadeIn -->
        </div>
        <!-- end container-fluid -->

        @foreach($data as $row)
            <div id="edit{{ $row->id }}" class="modal fade" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h6 class="modal-title">Edit Record</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <form action="/users/{{ $row->id }}" method="POST" id="needs-validation" novalidate="" enctype="multipart/form-data">
                        @csrf
                        @method('PATCH')

                        <div class="modal-body">
                            <div class="form-group">
                                <label>Username</label>
                                <input type="text" class="form-control" name="username" value="{{ $row->username }}" />
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" class="form-control" name="email" value="{{ $row->email }}" />
                            </div>
                            <div class="form-group">
                                <label>Full Name</label>
                                <input type="text" class="form-control" name="name" value="{{ $row->name }}" />
                            </div>
                            <div class="form-group">
                                <label>Role</label>
                                <select name="role" class="form-control select2" data-plugin="select2">
                                    @foreach($roles as $role)
                                        <option value="{{ $role->id }}" {{ $row->role === $role->id ? "Selected": "" }}>{{ $role->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        @endforeach
    </main>
@endsection
@foreach($data as $row)
    <div id="delete{{ $row->id }}" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title">Remove Record</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form action="/users/{{ $row->id }}" method="POST" id="needs-validation" novalidate="" enctype="multipart/form-data">
                    @csrf
                    @method('DELETE')

                    <div class="modal-body">
                        <h5>Delete This Record?</h5>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger">Remove</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endforeach
@foreach($data as $row)
    <div id="reset{{ $row->id }}" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title">Reset Password</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form action="/users/{{ $row->id }}" method="POST" id="needs-validation" novalidate="" enctype="multipart/form-data">
                    @csrf
                    @method('PATCH')

                    <div class="modal-body">
                        <div class="form-group">
                            <label>New Password</label>
                            <input type="text" class="form-control" name="password" required />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger">Reset Password</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endforeach
@section('script')
    <script>
        console.log('employee users')
    </script>
@endsection
