<?php

namespace App\Http\Controllers;

use App\Courses;
use App\Subcourse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CoursesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
       return view('Settings.courses', [
           'data'=>Courses::all()->sortBy('course')
       ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        Courses::create($request->all());
        $notification = array(
            'message' => 'Course Created Successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Courses  $courses
     * @return \Illuminate\Http\Response
     */
    public function show(Courses $courses)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Courses  $courses
     * @return \Illuminate\Http\Response
     */
    public function edit(Courses $courses)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        Courses::find($id)->update($request->all());
        $notification = array(
            'message' => 'Course Updated Successfully',
            'alert-type' => 'info'
        );
        return redirect()->back()->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        Courses::find($id)->delete();
        $notification = array(
            'message' => 'Course Deleted Successfully',
            'alert-type' => 'error'
        );
        return redirect()->back()->with($notification);
    }

    public function getSubCourse($course_id){

        $data = Subcourse::all()->where('course_id', $course_id);

        if(!empty($data)){
            foreach ($data as $row){

                $resp[] = array(
                    'value'=>$row->id,
                    "name"=>utf8_decode($row->sub_course_code . " " . $row->sub_course),
                );

            }
            return response()->json($resp);
        }
        $result[] = array(
            'name'=> "No record"
        );

        return response()->json($result);
    }

    public static function subCourse($sub_course_id){
        $sub_course = Subcourse::find($sub_course_id);

        if ($sub_course){
            return $sub_course->sub_course_code . " " . $sub_course->sub_course;
        }

        return 'NONE';
    }


}
