@extends('layout.app')

@section('content')
    <main class="main">
        <!-- Breadcrumb -->

        <div class="container-fluid">

            <div class="animated fadeIn">
                <div class="row mt-lg-5">
                    <div class="col-md-4">
                        <div class="card card-accent-theme">
                            <div class="card-header">
                                <h6 class="text-theme">Student Profile</h6>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <center>
                                            <a href="#">
                                                <div class="spinner"></div>
                                                <div class="img">
                                                    <img src="{{ $student->profile }}" alt="img">
                                                </div>
                                            </a>
                                        </center>
                                    </div>
                                </div>
                                <hr />
                                <div class="row">
                                    <div class="col-md-12">
                                        <h6>{{ ucwords($student->lastname . ", " . $student->firstname ." " . $student->middlename[0] . " " . $student->suffix . ".") }}</h6>
                                    </div>
                                </div>
                                <table class="table table-striped m-md-b-0">
                                    <tbody>
                                    <tr>
                                        <th scope="row">SRN</th>
                                        <td class="text-right">{{ $student->srn }}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Address</th>
                                        <td class="text-right">{{ $student->address }}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Contact Number</th>
                                        <td class="text-right">{{ $student->contact_number }}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Birth Date</th>
                                        <td class="text-right">{{ $student->date_of_birth }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                                @can('enroll-students')
                                <div class="row">
                                    <div class="col-md-12">
                                        <a href="#" data-toggle="modal" data-target="#enroll" class="btn btn-block btn-info text-white">Enroll Student</a>
                                    </div>
                                </div>
                                @endcan
                            </div>
                            <!-- end card-body -->
                        </div>
                        <!-- end card -->
                    </div>
                    <div class="col-md-8">
                        <div class="card card-accent-theme">
                            <div class="card-body">
                                <h4 class="text-theme">Student Information</h4>
                                <ul class="nav nav-tabs custom-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#info" role="tab" aria-controls="info" aria-selected="true">
                                            <i class="fa fa-user-circle-o"></i> Update Information</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#course" role="tab" aria-controls="info" aria-selected="true">
                                            <i class="fa fa-pencil-square-o"></i> Enrolled Course</a>
                                    </li>
                                </ul>

                                <div class="tab-content">
                                    <div class="tab-pane active" id="info" role="tabpanel">
                                        <div class="row">
                                            <form action="/students/{{ $student->id }}" method="POST">
                                                @csrf
                                                @method('PATCH')
                                                <div class="col-md-12">
                                                    <div class="form-group row offset-5">
                                                        <div class="col-md-6">
                                                            <label>Date</label>
                                                            <input type="text" name="enrollment_date" class="form-control" readonly value="{{  $student->enrollment_date }}" />
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label>Registration No</label>
                                                            <input type="text" class="form-control" name="registration_no" value="{{  $student->registration_no }}" readonly />
                                                        </div>
                                                    </div>
                                                    <fieldset class="form-group">
                                                        <legend>Personal Information</legend>
                                                        <div class="form-group row">
                                                            <div class="col-md-3">
                                                                <label>Last Name</label>
                                                                <input type="text" class="form-control" name="lastname" value="{{  $student->lastname  }}" placeholder="Enter Last Name" required/>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label>First Name</label>
                                                                <input type="text" class="form-control" name="firstname" value="{{  $student->firstname }}" placeholder="Enter First Name" required />
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label>Middle Name</label>
                                                                <input type="text" class="form-control" name="middlename" value="{{  $student->middlename }}" placeholder="Enter Middle Name" />
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label>Suffix</label>
                                                                <input type="text" class="form-control" name="suffix" value="{{  $student->suffix }}" placeholder="Enter Suffix"  />
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <div class="col-md-12">
                                                                <label>Address</label>
                                                                <textarea name="address" class="form-control" rows="4" required placeholder="Enter Address">{{  $student->address }}</textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <div class="col-md-3">
                                                                <label>Gender</label>
                                                                <select name="gender" class="form-control select2" data-plugin="select2" style="width: 100%" required>
                                                                    <option value="Male" {{ $student->gender === "Male"? "Selected": "" }}>Male</option>
                                                                    <option value="Female" {{ $student->gender === "Female"? "Selected": "" }}>Female</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label>Date of Birth</label>
                                                                <input type="date" class="form-control" name="date_of_birth" value="{{  $student->date_of_birth }}" required />
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label>Place of Birth</label>
                                                                <input type="text" class="form-control" name="place_of_birth" placeholder="Enter Place of Birth" value="{{  $student->place_of_birth }}" required />
                                                            </div>
                                                            <div class="col-md-3">
                                                                <label>Contact Number</label>
                                                                <input type="text" class="form-control" name="contact_number" placeholder="09xxxxxxxxx" required value="{{  $student->contact_number }}" />
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <div class="col-md-4">
                                                                <label>Rank</label>
                                                                <select name="rank_id" data-placeholder="Select Rank" class="form-control select2" data-plugin="select2" style="width: 100%;" readonly="">
                                                                    @foreach($ranks as $rank)
                                                                        <option value="{{ $rank->id }}" {{ $student->rank_id === $rank->id ? "Selected": "" }}>{{ $rank->rank }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <label>SRN</label>
                                                                <input type="number" class="form-control" name="srn" maxlength="9" minlength="1" placeholder="Enter your SRN" value="{{  $student->srn }}" required />
                                                            </div>
                                                            <div class="col-md-4">
                                                                <label>Endorser</label>
                                                                <select name="endorser_id" data-placeholder="Select Endorser" class="form-control select2" data-plugin="select2" style="width: 100%;" required>
                                                                    <option value="NONE">NONE</option>
                                                                    @foreach($endorsers as $endorser)
                                                                        <option value="{{ $endorser->id }}" {{ $student->endorser === $endorser->id? "selected" : "" }}>{{ $endorser->endorser_name }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                    <fieldset class="form-group">
                                                        <legend>In Case of Emergency</legend>
                                                        <div class="form-group row">
                                                            <div class="col-md-4">
                                                                <label>Contact Name</label>
                                                                <input type="text" class="form-control" name="contact_emergency_name" value="{{ $student->contact_emergency_name  }}" placeholder="Enter the name of your emergency contact" />
                                                            </div>
                                                            <div class="col-md-4">
                                                                <label>Contact Number</label>
                                                                <input type="text" class="form-control" name="contact_emergency_number" value="{{ $student->contact_emergency_number }}" placeholder="Enter Emergency Contact Number" />
                                                            </div>
                                                            <div class="col-md-4">
                                                                <label>Relationship</label>
                                                                <input type="text" class="form-control" name="contact_emergency_relationship" value="{{ $student->contact_emergency_relationship }}" placeholder="Enter Relationship to Contact" />

                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                    @can('edit-students')
                                                    <div class="form-group">
                                                        <button type="submit" class="btn btn-block btn-success"><i class="fa fa-save"></i> Update Record</button>
                                                    </div>
                                                    @endcan
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="course" role="tabpanel">
                                        <div class="row">
                                            @include('Students.enrolled-courses')
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- end-card-body -->
                        </div>
                        <!-- end card -->
                    </div>
                </div>
            </div>
            <!-- end animated fadeIn -->
        </div>
        <!-- end container-fluid -->
    </main>

    <div id="enroll" class="modal fade" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title">Select a Course</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form action="/enroll-student" method="POST" id="needs-validation" novalidate="" enctype="multipart/form-data">
                    @csrf

                    <div class="modal-body">
                       <div class="form-group">
                           <label>Course</label>
                           <input type="hidden" name="student_id" value="{{ $student->id }}" />
                           <select name="course_id" class="form-control select2" data-plugin="select2" data-placeholder="Select Courses" id="course_id" style="width: 100%" required>
                               <option value="" selected>Select Course</option>
                               @foreach($courses as $course)
                                   <option value="{{ $course->schedule_course_id }}">{{ $course->course . " - " . $course->schedule_course_id  }}</option>
                               @endforeach
                           </select>
                       </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Proceed to Enrollment</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    @foreach($enrollments as $row)
        <div id="removeEnrollment{{ $row->id }}" class="modal fade" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h6 class="modal-title">Remove Enrollment Record</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <form action="/enrollment/{{ $row->id }}" method="POST" id="needs-validation" novalidate="" enctype="multipart/form-data">
                        @csrf
                        @method('DELETE')

                        <div class="modal-body">
                            <p>Remove this student from {{ $row->course_code }}?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-danger" >Remove</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    @endforeach

@endsection
