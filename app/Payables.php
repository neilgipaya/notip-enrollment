<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payables extends Model
{
    //

    protected $table = "payables";

    protected $guarded = [];
}
