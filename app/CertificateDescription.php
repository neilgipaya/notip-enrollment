<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CertificateDescription extends Model
{
    //

    protected $table = "certificate_descriptions";

    protected $guarded = [];
}
