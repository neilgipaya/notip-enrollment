<?php

namespace App\Http\Controllers;

use App\Courses;
use App\Instructor;
use App\InstructorCourses;
use App\Subcourse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class InstructorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('Settings.instructor', [
            'data'=>Instructor::all()->sortBy('lastname')
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        Instructor::create($request->all());
        $notification = array(
            'message' => 'Instructor Created Successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return void
     */
    public function show($id)
    {
        //
        $instructor_courses = new InstructorCourses();
        $instructor = Instructor::find($id);

        $data = $instructor_courses->get_instructor_by_id($instructor->id);
        $courses = Courses::all();

        $sub_courses = Subcourse::all();

        $schedule = DB::table('class_schedules')
            ->join('schedules', 'schedules.id', '=', 'class_schedules.schedule_id')
            ->join('courses', 'courses.id', '=', 'schedules.course_id')
            ->where('class_schedules.instructor', $id)
            ->get();


        return view('Settings.instructor_courses', compact('instructor', 'data', 'courses', 'sub_courses', 'schedule'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Instructor  $courses
     * @return \Illuminate\Http\Response
     */
    public function edit(Instructor $courses)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        Instructor::find($id)->update($request->all());
        $notification = array(
            'message' => 'Instructor Updated Successfully',
            'alert-type' => 'info'
        );
        return redirect()->back()->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        Instructor::find($id)->delete();
        $notification = array(
            'message' => 'Instructor Deleted Successfully',
            'alert-type' => 'error'
        );
        return redirect()->back()->with($notification);
    }

    public static function getAssessor($id){
        $assessor = DB::table('instructors')
            ->join('instructor_courses', 'instructors.id', '=', 'instructor_courses.instructor_id')
            ->where('instructor_courses.position', '=', 'Assessor')
            ->where('instructors.id', $id)
            ->whereNull('instructor_courses.deleted_at')
            ->whereNull('instructors.deleted_at')
            ->first();

        if (!empty($assessor)) {
            $fullname = $assessor->lastname. ", ". $assessor->firstname. " " . $assessor->middlename[0];
            return $fullname;
        }

        return null;
    }

    public  static function getSuperVisor($id){
        $supervisor = DB::table('instructors')
            ->join('instructor_courses', 'instructors.id', '=', 'instructor_courses.instructor_id')
            ->where('instructor_courses.position', '=', 'Supervisor')
            ->where('instructors.id', $id)
            ->whereNull('instructor_courses.deleted_at')
            ->whereNull('instructors.deleted_at')
            ->first();

        if (!empty($supervisor)){
            $fullname = $supervisor->lastname. ", ". $supervisor->firstname. " " . $supervisor->middlename[0];
            return $fullname;
        }

        return null;
    }

    public static function countEnrollees($course_code){
        $count = DB::table('enrollments')->where('course_code', '=', $course_code)->count();

        return $count;
    }
}
