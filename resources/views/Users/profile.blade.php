@extends('layout.app')

@section('content')
    <main class="main">
        <!-- Breadcrumb -->

        <div class="container-fluid">

            <div class="animated fadeIn">
                <div class="row mt-lg-5">
                    <div class="col-md-4">
                        <div class="card card-accent-theme">
                            <div class="card-header">
                                <h6 class="text-theme">Basic Information</h6>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <p>{{ $user->name }}</p>
                                    </div>
                                </div>
                                <table class="table table-striped m-md-b-0">
                                    <tbody>
                                    <tr>
                                        <th scope="row">Email</th>
                                        <td class="text-right">{{ $user->email }}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Username</th>
                                        <td class="text-right">{{ $user->username }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- end card-body -->
                        </div>
                        <!-- end card -->
                    </div>
                    <div class="col-md-8">
                        <div class="card card-accent-theme">
                            <div class="card-body">
                                <h4 class="text-theme">User Profile</h4>
                                <ul class="nav nav-tabs custom-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#info" role="tab" aria-controls="info" aria-selected="true">
                                            <i class="fa fa-file"></i> Basic Info</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#home3" role="tab" aria-controls="home" aria-selected="false">
                                            <i class="fa fa-trash"></i> Change Password</a>
                                    </li>
                                </ul>

                                <div class="tab-content">
                                    <div class="tab-pane active" id="info" role="tabpanel">
                                        <div class="col-md-12">
                                            <div class="card card-accent-info">
                                                <div class="card-header bg-theme">
                                                    <p>My Profile</p>
                                                </div>
                                                <div class="card-body">
                                                    <form action="/profileUpdate/{{ $user->id }}" method="POST" enctype="multipart/form-data">
                                                        @csrf
                                                        @method('PATCH')
                                                        <div class="form-group">
                                                            <label>Username</label>
                                                            <input type="text" class="form-control" name="username" value="{{ $user->username }}" readonly />
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Email</label>
                                                            <input type="email" class="form-control" name="email" value="{{ $user->email }}" />
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Full Name</label>
                                                            <input type="text" class="form-control" name="name" value=" {{ $user->name }}" />
                                                        </div>
                                                        <div class="form-group">
                                                            <button type="submit" class="btn btn-block btn-success">Update Profile</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="home3" role="tabpanel">
                                        <h5>Change Password</h5>
                                        <form action="changePassword" method="POST">
                                            @csrf
                                            <div class="form-group">
                                                <label>Current Password</label>
                                                <input type="password" class="form-control" name="current-password" required />
                                            </div>
                                            <div class="form-group">
                                                <label>New Password</label>
                                                <input type="password" class="form-control" name="password" required />
                                            </div>
                                            <div class="form-group">
                                                <label>Retype Password</label>
                                                <input type="password" class="form-control" name="re-password" required />
                                            </div>
                                            <div class="form-group">
                                                <button class="btn btn-dark btn-block" type="submit">Reset Password</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                            </div>
                            <!-- end-card-body -->
                        </div>
                        <!-- end card -->
                    </div>
                </div>
            </div>
            <!-- end animated fadeIn -->
        </div>
        <!-- end container-fluid -->
    </main>
@endsection

@section('script')
    <script>
        console.log('employee users')
    </script>
    <script>
        $("#summernote-1").summernote({
            height: 150
        });
    </script>
@endsection
