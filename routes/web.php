<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware'=>'guest'], function () {

    Route::get(
        '/',
        function () {
            return view('login');
        }
    )->name('login');

    Route::get(
        'logout',
        function () {
            Auth::logout();

            return view('login');
        }
    );

    Route::post('authentication', 'AuthenticationController@login');

});




Route::group(['middleware' => 'auth'], function () {

    Route::get('logout', function () {
            Auth::logout();

            return view('login');
        }
    );

    //printAttendance
    Route::get('printAttendance/{course_id}', 'AttendanceSheetController@printAttendance');

    //subcourse

    Route::get('testExport/{file_name}', 'ExcelExport@testExport');
    Route::post('export', 'ExcelExport@export');
    Route::get('/getSubCourse/{course_id}', 'CoursesController@getSubCourse');

    //certificate
    Route::get('releasing', 'CertificateController@releasing');
    Route::post('certificatePrinting', 'CertificateController@certificatePrinting');

    //enrollment
    Route::get('/printTRS/{student_id}', 'EnrollmentController@printTRS');

    //profile
    Route::get('profile', 'AuthenticationController@profile');
    Route::patch('profileUpdate/{id}', 'AuthenticationController@profileUpdate');
    Route::post('changePassword', 'AuthenticationController@changePassword');

    //ajax
    Route::get('getAssessor/{course_id}', 'ScheduleController@getAssessor');
    Route::get('getSupervisor/{course_id}', 'ScheduleController@getSupervisor');
    Route::get('getInstructor/{course_id}', 'ScheduleController@getInstructorList');

    //payment
    Route::resource('payment', 'PayablesController');
    Route::get('pendingPayment', 'PayablesController@pendingPayment');
    Route::get('paymentCompletion', 'PayablesController@paymentCompletion');
    Route::get('waitingList', 'PayablesController@waitingList');
    Route::get('paymentPaid', 'PayablesController@paymentPaid');

    Route::post('enroll-student', 'EnrollmentController@enroll');
    Route::get('studentEnrollment/{course_id}/{student_id}', 'EnrollmentController@studentEnrollment');
    Route::get('/enrollment-success/{student_id}', 'EnrollmentController@enrollmentSuccess');


    Route::get('reservation', 'StudentsController@reservation');
    Route::get('reservedStudents', 'StudentsController@reservedStudents');
    Route::resource('dashboard', 'DashboardController');
    Route::resource('enrollment', 'EnrollmentController');
    Route::resource('students', 'StudentsController');
    Route::resource('schedule', 'ScheduleController');
    Route::resource('ranks', 'RanksController');
    Route::resource('courses', 'CoursesController');
    Route::resource('rooms', 'RoomsController');
    Route::resource('instructor', 'InstructorController');
    Route::resource('instructor-courses', 'InstructorCoursesController');
    Route::resource('class-schedule', 'ClassScheduleController');
    Route::resource("enrollment", 'EnrollmentController');
    Route::resource('endorser', 'EndorserController');
    Route::resource('users', 'UsersController');
    Route::resource('roles', 'RolesController');
    Route::resource('certificates', 'CertificateController');
    Route::resource('sub-courses', 'SubcourseController');
    Route::resource('certificate-setup', 'CertificateDescriptionController');
    Route::resource('attendance-sheet', 'AttendanceSheetController');

});
