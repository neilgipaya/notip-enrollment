<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <meta name="description" content="Admin, Dashboard, Bootstrap" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <title>NOTIP</title>

    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('assets') }}/img/favicon/apple-touch-icon.png" />
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('assets') }}/img/favicon/favicon-32x32.png" />
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets') }}/img/favicon/favicon-16x16.png" />
    <link rel="manifest" href="{{ asset('assets') }}/img/favicon/manifest.json" />
    <link rel="mask-icon" href="{{ asset('assets') }}/img/favicon/safari-pinned-tab.svg" color="#5bbad5" />
    <meta name="theme-color" content="#ffffff" />

    <!-- fonts -->
    <link rel="stylesheet" href="{{ asset('assets') }}/fonts/md-fonts/css/materialdesignicons.min.css" />
    <link rel="stylesheet" href="{{ asset('assets') }}/fonts/font-awesome-4.7.0/css/font-awesome.min.css" />
    <!-- animate css -->
    <link rel="stylesheet" href="{{ asset('assets') }}/libs/animate.css/animate.min.css" />

    <!-- jquery-loading -->
    <link rel="stylesheet" href="{{ asset('assets') }}/libs/jquery-loading/dist/jquery.loading.min.css" />
    <!-- octadmin main style -->
    <link id="pageStyle" rel="stylesheet" href="{{ asset('assets') }}/css/style-xing.css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>

<body>
<section class="container-pages">

    <div class="brand-logo float-left">
        <a class="" href="#">
            NOTIP
        </a>
    </div>


    <div class="card pages-card col-lg-4 col-md-6 col-sm-6">
        <div class="card-body ">
            <div class="h4 text-center text-theme"><strong>Login</strong></div>
            <div class="small text-center text-dark"> Login to Account </div>

            <form action="/authentication" method="POST">
                @csrf
                <div class="form-group">
                    <div class="input-group">
                                     <span class="input-group-addon text-theme"><i class="fa fa-user"></i>
                                    </span>
                        <input type="text" id="username" name="username" class="form-control" placeholder="Username" />
                    </div>
                </div>

                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon text-theme"><i class="fa fa-asterisk"></i></span>
                        <input type="password" id="password" name="password" class="form-control" placeholder="Password" />

                    </div>
                </div>
                <div class="form-group form-actions">
                    <button type="submit" class="btn  btn-theme login-btn ">   Login   </button>
                </div>
            </form>
        </div>
        <!-- end card-body -->
    </div>
    <!-- end card -->
</section>
<!-- end section container -->
<div class="half-circle"></div>
<div class="small-circle"></div>

<div id="copyright"></div>

<!-- Bootstrap and necessary plugins -->
<script src="{{ asset('assets') }}/libs/jquery/dist/jquery.min.js"></script>
<script src="{{ asset('assets') }}/libs/popper.js/dist/umd/popper.min.js"></script>
<script src="{{ asset('assets') }}/libs/bootstrap/bootstrap.min.js"></script>
<script src="{{ asset('assets') }}/libs/PACE/pace.min.js"></script>
<script src="{{ asset('assets') }}/libs/chart.js/dist/Chart.min.js"></script>
<script src="{{ asset('assets') }}/libs/nicescroll/jquery.nicescroll.min.js"></script>

<script src="{{ asset('assets') }}/libs/jquery-knob/dist/jquery.knob.min.js"></script>


<!-- jquery-loading -->
<script src="{{ asset('assets') }}/libs/jquery-loading/dist/jquery.loading.min.js"></script>
<script src="{{ asset('assets') }}/libs/toastr/toastr.min.js"></script>


<!-- octadmin Main Script -->
<script src="{{ asset('assets') }}/js/app.js"></script>
<script>
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "1000",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
        @if(Session::has('message'))
    var type = "{{ Session::get('alert-type', 'info') }}";
    switch(type){
        case 'info':
            toastr.info("{{ Session::get('message') }}");
            break;

        case 'warning':
            toastr.warning("{{ Session::get('message') }}");
            break;

        case 'success':
            toastr.success("{{ Session::get('message') }}");
            break;

        case 'error':
            toastr.error("{{ Session::get('message') }}");
            break;
    }
    @endif
</script>
@if ($errors->any())
    @foreach ($errors->all() as $error)
        <script>
            toastr.warning('{{ $error }}');
        </script>
    @endforeach
@endif

</body>

</html>
