<?php

namespace App\Http\Controllers;

use App\Courses;
use App\ErrorLogs;
use App\Ranks;
use App\ScheduleLogs;
use App\StudentLogs;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class GeneralUtil extends Controller
{
    //

    public function diffInDate($param){

        $date = Carbon::parse($param[0]);
        $diff = $date->diffInDays($param[1]) + 1;

        if(!empty($diff)){
            return $diff;
        }
        return [];
    }

    public function diffInMonths($param){

        $date = Carbon::parse($param[0]);
        $diff = $date->diffInMonths($param[1]);

        if(!empty($diff)){
            return $diff;
        }
        return [];
    }

    public function generateScheduleLogs($action, $course_code){
        ScheduleLogs::create(
            [
                'user_id' => Auth::user()->id,
                'course_code' => $course_code,
                'action' => $action,
                'date_time' => now()
            ]
        );
    }

    public function errorLogs($model, $code, $message){
        ErrorLogs::create([
           'model' => $model,
           'error_code' => $code,
            'error_message' => $message
        ]);
    }

    public function studentLogs($student_id, $action){
        StudentLogs::create([
            'student_id' => $student_id,
            'action' => $action
        ]);

    }

    public function getCourseById($id){
        $query = Courses::find($id);

        if ($query){
            return $query;
        }

        return [];
    }

    public function getRankById($id){
        $ranks = Ranks::find($id);

        if (isset($ranks)){
            return $ranks;
        }

        return [];

    }
}
