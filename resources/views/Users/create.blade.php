@extends('layout.app')

@section('content')
<main class="main">
    <!-- Breadcrumb -->

    <div class="container-fluid">

        <div class="animated fadeIn">

            <div class="row mt-lg-5">
                <div class="col-md-6 offset-3">
                    <div class="card card-accent-info">
                        <div class="card-header bg-theme">
                            <p>Create New Record</p>
                        </div>
                        <div class="card-body">
                            <form action="/users" method="POST" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="role" value="1" />
                                <div class="form-group">
                                    <label>Username</label>
                                    <input type="text" class="form-control" name="username" value="{{ old('username') }}" />
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input type="password" class="form-control" name="password" />
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" class="form-control" name="email" value="{{ old('email') }}" />
                                </div>
                                <div class="form-group">
                                    <label>Full Name</label>
                                    <input type="text" class="form-control" name="name" value="{{ old('name') }}" />
                                </div>
                                <div class="form-group">
                                    <label>Role</label>
                                    <select name="role" class="form-control select2" data-plugin="select2">
                                       @foreach($roles as $role)
                                           <option value="{{ $role->id }}">{{ $role->name }}</option>
                                       @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-block btn-success"><i class="fa fa-save"></i> Save Record</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end animated fadeIn -->
    </div>
    <!-- end container-fluid -->

</main>
@endsection

@section('script')
    <script>
        console.log('create users')
    </script>
@endsection
