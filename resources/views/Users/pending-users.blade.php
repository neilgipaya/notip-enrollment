@extends('layout.app')

@section('content')
    <main class="main">
        <!-- Breadcrumb -->

        <div class="container-fluid">

            <div class="animated fadeIn">

                <div class="row mt-lg-5">
                    <div class="col-md-12">
                        <div class="card card-accent-theme">
                            <div class="card-body">
                                <h4 class="text-theme">
                                    Pending Users
                                </h4>
                                <table class="table table-hover dataTable table-striped w-full" data-plugin="dataTable" width="100%">
                                    <thead>
                                    <tr>
                                        <th>Username</th>
                                        <th>Email</th>
                                        <th>Full Name</th>
                                        <th>Gender</th>
                                        <th>Address</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @foreach($users as $row)
                                        <tr>
                                            <td>{{ $row->username }}</td>
                                            <td>{{ $row->email }}</td>
                                            <td>{{ $row->lastname .", ". $row->firstname }}</td>
                                            <td>{{ $row->gender }}</td>
                                            <td>{{ $row->address }}</td>
                                            <td>{{ $row->status }}</td>

                                            <td>
                                                <a href="" data-target="#approve{{ $row->id }}" data-toggle="modal" class="btn
                                                        btn-xs
                                                        btn-warning" style="margin-top: -5px"title="Edit Record"><i class="fa fa-thumbs-o-up"
                                                                                                                    style="color: black;"></i> </a>
                                                <a href="" data-target="#delete{{ $row->id }}" data-toggle="modal" class="btn
                                                        btn-xs  btn-danger" style="margin-top: -5px" title="Delete Record"><i class="fa fa-close"
                                                                                                                              style="color: white;"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                            </div>
                            <!-- end card-body -->
                        </div>
                        <!-- end card -->
                    </div>
                </div>
            </div>
            <!-- end animated fadeIn -->
        </div>
        <!-- end container-fluid -->

        @foreach($users as $row)
            <div id="approve{{ $row->id }}" class="modal fade" role="dialog" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h6 class="modal-title">Approve Record</h6>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <form action="/users/{{ $row->id }}" method="POST" id="needs-validation" novalidate="" enctype="multipart/form-data">
                            @csrf
                            @method('PATCH')

                            <div class="modal-body">
                                <p>Are you sure you want to approve this user?</p>
                                <input type="hidden" name="status" value="Approve" />
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Approve User</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
        @endforeach
    </main>
@endsection
@foreach($users as $row)
    <div id="decline{{ $row->id }}" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title">Decline Record</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form action="/users/{{ $row->id }}" method="POST" id="needs-validation" novalidate="" enctype="multipart/form-data">
                    @csrf
                    @method('PATCH')
                    <div class="modal-body">
                        <input type="hidden" name="status" value="Declined" />
                        <p>Are you sure you want to decline this user?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger">Remove</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endforeach
@section('script')
    <script>
        console.log('employee users')
    </script>
@endsection
