<?php

namespace App\Http\Controllers;

use App\Courses;
use App\Subcourse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;

class SubcourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        Subcourse::create($request->all());
        $notification = array(
            'message' => 'Course Created Successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Subcourse  $subcourse
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

        $course = Courses::findOrFail($id);

        $data = Subcourse::all()->where('course_id', $id);

        return view('Settings.sub-courses', compact('data', 'course'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Subcourse  $subcourse
     * @return \Illuminate\Http\Response
     */
    public function edit(Subcourse $subcourse)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Subcourse  $subcourse
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        Subcourse::find($id)->update($request->all());
        $notification = array(
            'message' => 'Course Updated Successfully',
            'alert-type' => 'info'
        );
        return redirect()->back()->with($notification);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Subcourse  $subcourse
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        Subcourse::find($id)->delete();
        $notification = array(
            'message' => 'Course Deleted Successfully',
            'alert-type' => 'warning'
        );
        return redirect()->back()->with($notification);
    }
}
