@extends('layout.app')

@section('content')
    <main class="main">
        <!-- Breadcrumb -->
        <ol class="breadcrumb bc-colored bg-theme" id="breadcrumb">
            <li class="breadcrumb-item ">
                <a href="">Students</a>
            </li>
            <li class="breadcrumb-item">
                <a href="#"> Manage Students </a>
            </li>
        </ol>

        <div class="container-fluid">

            <div class="animated fadeIn">
                <div class="row">

                    <div class="col-md-12">
                        <div class="card card-accent-theme">

                            <div class="card-body">
                                <h4 class="text-theme">Students

                                </h4>
                                <br />
                                <table class="display table table-hover table-striped dataTable" data-plugin="datatable" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th>Student No</th>
                                        <th>TRS No</th>
                                        <th>SRN</th>
                                        <th>Enrollment Date</th>
                                        <th>Full Name </th>
                                        <th>Gender</th>
                                        <th>Address</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($data as $row)
                                        <tr>
                                            <td>{{ $row->registration_no }}</td>
                                            <td>{{ $row->trs_no }}</td>
                                            <td>{{ $row->srn }}</td>
                                            <td>{{ $row->enrollment_date }}</td>
                                            <td>{{ ucwords($row->lastname . ", " . $row->firstname . " " . $row->middlename[0] .".") }}</td>
                                            <td>{{ $row->gender}}</td>
                                            <td>{{ $row->address}}</td>
                                            <td class="text-nowrap" style="margin: 0">
                                                @can('view-student-info')
                                                <a class="btn btn-info btn-sm m-0" href="{{ url('/students/' . $row->id) }}" title="View More">
                                                    <i class="fa fa-eye"></i>
                                                </a>
                                                <a class="btn btn-warning btn-sm m-0" title="Print TRS" href="{{ url('printTRS/'.$row->id) }}" target="_blank">
                                                    <i class="fa fa-print"></i>
                                                </a>
                                                @endcan
                                                @can('delete-students')
                                                <a class="btn btn-danger btn-sm m-0" title="Delete" href="#" data-toggle="modal" data-target="#delete{{ $row->id }}" >
                                                    <i class="fa fa-close"></i>
                                                </a>
                                                @endcan
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- end card-body -->
                        </div>
                        <!-- end card -->
                    </div>
                    <!-- end col -->

                </div>
                <!-- end row -->
            </div>
            <!-- end animated fadeIn -->
        </div>
        <!-- end container-fluid -->
    </main>
    <!-- end main -->

    @foreach($data as $row)
        <div id="delete{{ $row->id }}" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h6 class="modal-title">Remove Record</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <form action="/students/{{ $row->id }}" method="POST" id="needs-validation" novalidate="" enctype="multipart/form-data">
                        @csrf
                        @method('DELETE')

                        <div class="modal-body">
                            <h5>Delete This Record?</h5>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-danger">Remove</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    @endforeach


@endsection

@section('script')

@endsection
