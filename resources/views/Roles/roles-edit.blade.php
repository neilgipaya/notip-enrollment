@extends('layout.app')

@section('content')
    <main class="main">
        <!-- Breadcrumb -->

        <div class="container-fluid">

            <div class="animated fadeIn">

                <div class="row mt-lg-5">
                    <div class="col-md-6 offset-3">
                        <div class="card card-accent-info">
                            <div class="card-header bg-theme">
                                <p>Update Roles and Permission</p>
                            </div>
                            <div class="card-body">
                                <form action="/roles/{{ $role->id }}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    @method('PATCH')
                                    <div class="form-group">
                                        <label>Role Name</label>
                                        <input type="text" class="form-control" value="{{ $role->name }}" name="role" required />
                                    </div>
                                    <div class="form-group">
                                        <label>Permissions</label>
                                        @foreach($permissions as $perm)
                                            <div class="form-check">
                                                <input type="checkbox" name="permission[]" value="{{ $perm->id }}" @if(in_array($perm->id, $rolePermissions)) checked @endif /> &nbsp;&nbsp;&nbsp;
                                                <label>{{ ucwords(str_replace('-', ' ', $perm->name)) }}</label>
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-block btn-success"><i class="fa fa-save"></i> Save Record</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end animated fadeIn -->
        </div>
        <!-- end container-fluid -->

    </main>
@endsection

@section('script')

@endsection
