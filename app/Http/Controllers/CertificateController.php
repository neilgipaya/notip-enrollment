<?php

namespace App\Http\Controllers;

use App\Certificate;
use App\CertificateDescription;
use App\ClassSchedule;
use App\Courses;
use App\Endorser;
use App\Ranks;
use App\Rooms;
use App\Schedule;
use App\ScheduleLogs;
use App\Students;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use setasign\Fpdi\Fpdi;

class CertificateController extends Controller
{

    const TEMPIMGLOC = 'tempimg.png';


    /**
     * @var GeneralUtil
     */
    private $generalUtil;

    /**
     * CertificateController constructor.
     * @param GeneralUtil $generalUtil
     */
    public function __construct(GeneralUtil $generalUtil)
    {
        $this->generalUtil = $generalUtil;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $format = CertificateDescription::all();

        $data = DB::table('courses')
            ->join('schedules', 'schedules.course_id', '=', 'courses.id')
            ->whereNull('courses.deleted_at')
            ->get();



        return view('Certificate.certificate-list', compact('data', 'format'));


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return void
     */
    public function show($id)
    {
        //

        try{
            $schedule = DB::table('courses')
                ->join('schedules', 'schedules.course_id', '=', 'courses.id')
                ->whereNull('courses.deleted_at')
                ->where('schedules.id', '=', $id)
                ->first();


            $instructors = DB::table('instructor_courses')
                ->join('instructors', 'instructors.id', '=', 'instructor_courses.instructor_id')
                ->where('instructor_courses.course_id', $schedule->course_id)
                ->where('instructor_courses.position', 'Instructor')
                ->whereNull('instructor_courses.deleted_at')
                ->whereNull('instructors.deleted_at')
                ->get();

            $class_schedule = ClassSchedule::all()->where('schedule_id', $id);

            $rooms  = Rooms::all()->sortByDesc('id');


            $enrolled_students = DB::table('students')
                ->join('enrollments', 'students.id', '=', 'enrollments.student_id')
                ->where('enrollments.course_code', $schedule->schedule_course_id)
                ->get();

            $schedule_history = ScheduleLogs::all()->take(20)->where('course_code', $schedule->schedule_course_id);

            $schedule_list = Schedule::all();

            $assessors  = DB::table('instructors')
                ->join('instructor_courses', 'instructors.id', '=', 'instructor_courses.instructor_id')
                ->where('instructor_courses.position', '=', 'Assessor')
                ->whereNull('instructor_courses.deleted_at')
                ->whereNull('instructors.deleted_at')
                ->get();

            $supervisors  = DB::table('instructors')
                ->join('instructor_courses', 'instructors.id', '=', 'instructor_courses.instructor_id')
                ->where('instructor_courses.position', '=', 'Supervisor')
                ->whereNull('instructor_courses.deleted_at')
                ->whereNull('instructors.deleted_at')
                ->get();

            $format = CertificateDescription::all();

            return view('Certificate.certificate-information', compact('schedule', 'assessors', 'supervisors', 'class_schedule', 'rooms', 'instructors', 'enrolled_students', 'schedule_history', 'schedule_list', 'format'));
        }
        catch (\Exception $e){
            $notification = array(
                'message' => 'Fatal Error, Please Contact Administrator',
                'alert-type' => 'error'
            );

            $this->generalUtil->errorLogs("Certificate", $e->getCode(), $e->getMessage());
            return redirect()->back()->with($notification);
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Certificate  $certificate
     * @return \Illuminate\Http\Response
     */
    public function edit(Certificate $certificate)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Certificate  $certificate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Certificate $certificate)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Certificate  $certificate
     * @return \Illuminate\Http\Response
     */
    public function destroy(Certificate $certificate)
    {
        //
    }

    /**
     * Releasing of Grades
     *
     * @param  \App\Certificate  $certificate
     * @return \Illuminate\Http\Response
     */

    public function releasing(){
        //

        $data = DB::table('courses')
            ->join('schedules', 'schedules.course_id', '=', 'courses.id')
            ->whereNull('courses.deleted_at')
            ->get();


        return view('Releasing.realeasing-list', compact('data'));
    }

    /**
     * Printing of Certificates
     *
     * @param Request $request
     * @return string
     */
    public function certificatePrinting(Request $request)
    {


        try{

            $student = Students::find($request->post('student_id'));

            $format = CertificateDescription::find($request->post('format'));

            $schedule = DB::table('schedules')
                ->where('schedule_course_id', $request->post('enrollment_course_id'))
                ->first();


            $pdf = new Fpdi();

            $file = public_path('certificate-template.pdf');


            $pageCount = $pdf->setSourceFile($file);
// iterate through all pages
            $tpl = $pdf->importPage(1);
            $pdf->AddPage();

            $pdf->useTemplate($tpl);

            $pdf->SetFont('Arial');
            $pdf->SetFontSize('11'); // set font size

            //x = left right
            //y == taas baba

            $pdf->SetXY(158, 51.5); // set the position of the box
            $pdf->Cell(0, 10, $format->certificate_no, 'C');

            $pdf->SetXY(160, 56.5); // set the position of the box
            $pdf->Cell(0, 10, $request->post('registration_number'), 'C');

            $name = strtoupper($student->firstname . " " . $student->middlename[0] . ". " . $student->lastname);

            $pdf->SetFont('Arial', 'B');
            $pdf->SetFontSize('18'); // set font size
            $pdf->SetXY(10, 115); // set the position of the box
            $pdf->Cell(0, 10, $name , '0', '0', 'C');

            $pdf->SetFont('Arial', 'B');
            $pdf->SetFontSize('14'); // set font size
            $pdf->SetXY(10, 140); // set the position of the box
            $pdf->Cell(0, 10, $this->generalUtil->getCourseById($format->course_id)->course , '0', '0', 'C');

            $content = strip_tags($format->certificate_description);

//            $issuance_date = Carbon::today()->format('dS') . " of " . Carbon::today()->format('F, Y');

            $conducted_date = date('F d, Y', strtotime($schedule->schedule_from)) . " to " . date('F d, Y', strtotime($schedule->schedule_to));


            $stripped = str_replace(array("{conducted_date}"), array($conducted_date), $content);

            $pdf->SetFont('Arial');
            $pdf->SetFontSize('11'); // set font size
            $pdf->SetXY(20, 160); // set the position of the box
            $pdf->MultiCell(0, 5,  html_entity_decode($stripped, ENT_HTML5, 'UTF-8'), 0, 'C');

            $issuance = "Issued this " . Carbon::today()->format('dS') . " of " . Carbon::today()->format('F, Y'). " in the City of Manila, Philippines";

            $pdf->SetFont('Arial');
            $pdf->SetFontSize('11'); // set font size
            $pdf->SetXY(20, 195); // set the position of the box
            $pdf->MultiCell(0, 5, $issuance , 0, 'C');



//            $pdf->SetFont('Arial');
//            $pdf->SetFontSize('11'); // set font size
//            $pdf->SetXY(90, 160); // set the position of the box

            if (!empty($student->profile)){
                $pic = $this->getImage($student->profile);
                if ($pic!==false) $pdf->Image($pic[0], 90,208,30,25, $pic[1]);
            }

            return $pdf->Output();


        } catch (Exception $e){
            dd($e->getLine() . $e->getMessage());
            dd($e->getMessage() . $e->getLine());
            $notification = array(
                'message' => 'Fatal Error, Please Contact Administrator',
                'alert-type' => 'error'
            );

            $this->generalUtil->errorLogs("Enrollment", $e->getCode(), $e->getMessage());
            return redirect()->back()->with($notification);
        }



    }

    private function getImage($dataURI){
        $img = explode(',',$dataURI,2);
        $pic = 'data://text/plain;base64,'.$img[1];
        $type = explode("/", explode(':', substr($dataURI, 0, strpos($dataURI, ';')))[1])[1]; // get the image type
        if ($type=="png"||$type=="jpeg"||$type=="gif") return array($pic, $type);
        return false;
    }
}
