<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mockery\Exception;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolesController extends Controller
{
    /**
     * @var GeneralUtil
     */
    private $generalUtil;

    /**
     * RolesController constructor.
     * @param GeneralUtil $generalUtil
     */
    public function __construct(GeneralUtil $generalUtil)
    {
        $this->generalUtil = $generalUtil;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $data = Role::all();

        return view('Roles.roles', compact('data'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $permissions = Permission::all();

        return view('Roles.roles-create', compact('permissions'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        try{
            $name = $request->post('role');

            $role = Role::create(['name'=>$name]);
            $role->syncPermissions($request->input('permission'));

            $notification = array(
                'message' => 'Role Created Successfully',
                'alert-type' => 'success'
            );
            return redirect()->back()->with($notification);
        } catch (Exception $e){
            $notification = array(
                'message' => 'Fatal Error, Please Contact Administrator',
                'alert-type' => 'error'
            );

            $this->generalUtil->errorLogs("Roles", $e->getCode(), $e->getMessage());
            return redirect()->back()->with($notification);
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

        $role = Role::findOrFail($id);
        $rolePermissions = Role::findOrFail($id)->permissions->pluck('id')->toArray();
        $permissions = Permission::all();

        return view('Roles.roles-edit', compact('permissions', 'role', 'rolePermissions'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $role1 = Role::find($id)->update([
            'name'=>$request->post('role')
        ]);

        $role = Role::find($id);

        $role->syncPermissions($request->except('_token', '_method', 'role'));

        $notification = array(
            'message' => 'Role Updated Successfully',
            'alert-type' => 'info'
        );
        return redirect()->back()->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
