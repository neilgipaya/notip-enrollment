@extends('layout.app')

@section('content')

    @inject('courseController', 'App\Http\Controllers\CoursesController')

    <main class="main">
        <!-- Breadcrumb -->

        <div class="container-fluid">

            <div class="animated fadeIn">
                <div class="row mt-lg-5">
                    <div class="col-md-4">
                        <div class="card card-accent-theme">
                            <div class="card-header">
                                <h6 class="text-theme">Basic Information</h6>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <p>{{ $instructor->firstname . " " . $instructor->lastname }}</p>
                                    </div>
                                </div>
                                <table class="table table-striped m-md-b-0">
                                    <tbody>
                                    <tr>
                                        <th scope="row">Contact No</th>
                                        <td class="text-right">{{ $instructor->contact_no }}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Date Hired</th>
                                        <td class="text-right">{{ $instructor->date_hired }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- end card-body -->
                        </div>
                        <!-- end card -->
                    </div>
                    <div class="col-md-8">
                        <div class="card card-accent-theme">
                            <div class="card-body">
                                <h4 class="text-theme">My Courses</h4>
                                <ul class="nav nav-tabs custom-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#info" role="tab" aria-controls="info" aria-selected="true">
                                            <i class="fa fa-file"></i>Accreditation List</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#workload" role="tab" aria-controls="workload" aria-selected="true">
                                            <i class="fa fa-calendar"></i>Workload</a>
                                    </li>
                                </ul>

                                <div class="tab-content">
                                    <div class="tab-pane active" id="info" role="tabpanel">
                                        <div class="col-md-12">
                                            <div align="right" >
                                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#create">Create </button>
                                            </div>
                                            <table class="display table table-hover table-striped dataTable" data-plugin="datatable" cellspacing="0" width="100%">
                                                <thead>
                                                <tr>
                                                    <th>Course </th>
                                                    <th>Course Code</th>
                                                    <th>Sub Course</th>
                                                    <th>Position</th>
                                                    <th>Accreditation Expiration</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($data as $row)
                                                    <tr>
                                                        <td>{{ $row->course }}</td>
                                                        <td>{{ $row->course_code }}</td>
                                                        <td>{{ $courseController::subCourse($row->sub_course_id) }}</td>
                                                        <td>{{ $row->position }}</td>
                                                        <td>{{ $row->accreditation_expiration }}</td>
                                                        <td class="text-nowrap" style="margin: 0">

                                                            <a class="btn btn-warning btn-sm m-0" title="Edit" href="#" data-toggle="modal" data-target="#edit{{ $row->id }}">
                                                                <i class="fa fa-pencil"></i>
                                                            </a>
                                                            <a class="btn btn-danger btn-sm m-0" title="Delete" href="#" data-toggle="modal" data-target="#delete{{ $row->id }}" >
                                                                <i class="fa fa-close"></i>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="workload" role="tabpanel">
                                        <div class="col-md-12">
                                            <table class="display table table-hover table-striped dataTable" data-plugin="datatable" cellspacing="0" width="100%">
                                                <thead>
                                                <tr>
                                                    <th>Course </th>
                                                    <th>Course Code</th>
                                                    <th>Sub Course</th>
                                                    <th>Schedule</th>
                                                    <th>Time</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($schedule as $row)
                                                    <tr>
                                                        <td>{{ $row->course }}</td>
                                                        <td>{{ $row->course_code }}</td>
                                                        <td>{{ $courseController::subCourse($row->schedule_sub_course_id) }}</td>
                                                        <td>{{ date('F d, Y', strtotime($row->date))  }}</td>
                                                        <td>{{ $row->time_from . " - " . $row->time_to }}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- end-card-body -->
                        </div>
                        <!-- end card -->
                    </div>
                </div>
            </div>
            <!-- end animated fadeIn -->
        </div>
        <!-- end container-fluid -->
    </main>



    <div id="create" class="modal fade" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title">Create Record</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form action="/instructor-courses" method="POST" id="needs-validation" novalidate="" enctype="multipart/form-data">
                    @csrf

                    <div class="modal-body">

                        <input type="hidden" name="instructor_id" value="{{ $instructor->id }}" />
                        <div class="form-group">
                            <label>Course</label>
                            <select name="course_id" id="course_id" class="form-control select2" data-plugin="select2" style="width: 100%" onchange="getSubCourse()">
                                <option value="">Select Course</option>
                                @foreach($courses as $c)
                                    <option value="{{ $c->id }}">{{ $c->course . " - " . $c->course_code }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Sub Course</label>
                            <select name="sub_course_id" id="sub_course" class="form-control select2" data-plugin="select2" style="width: 100%">

                            </select>
                        </div>
                        <div class="form-group">
                            <label>Position</label>
                            <select name="position" class="form-control select2" data-plugin="select2" style="width: 100%;">
                                <option value="Instructor">Instructor</option>
                                <option value="Assessor">Assessor</option>
                                <option value="Supervisor">Supervisor</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Accreditation Expiration</label>
                            <input type="date" class="form-control" name="accreditation_expiration" required />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save Record</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    @foreach($data as $row)
        <div id="edit{{ $row->id }}" class="modal fade" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h6 class="modal-title">Edit Record</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>pixel
                    <form action="/instructor-courses/{{ $row->id }}" method="POST" id="needs-validation" novalidate="" enctype="multipart/form-data">
                        @csrf
                        @method('PATCH')

                        <div class="modal-body">
                            <select name="course_id" id="course_id" class="form-control select2" data-plugin="select2" style="width: 100%" onchange="getSubCourse()">
                                <option value="">Select Course</option>
                                @foreach($courses as $c)
                                    <option value="{{ $c->id }}">{{ $c->course . " - " . $c->course_code }}</option>
                                @endforeach
                            </select>
                            <div class="form-group">
                                <label>Sub Course</label>
                                <select name="sub_course_id" id="sub_course" class="form-control select2" data-plugin="select2" style="width: 100%">
                                    @foreach($sub_courses as $sub)
                                        <option value="{{ $sub->id }}" {{ $row->sub_course_id == $sub->id? "Selected": "" }}>{{ $sub->sub_course_code . " " . $sub->sub_course }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Position</label>
                                <select name="position" class="form-control select2" data-plugin="select2" style="width: 100%;">
                                    <option value="Instructor" {{ $row->position === "Instructor"? "Selected" : "" }}>Instructor</option>
                                    <option value="Assessor" {{ $row->position === "Assessor"? "Selected" : "" }}>Assessor</option>
                                    <option value="Supervisor" {{ $row->position === "Supervisor"? "Selected" : "" }}>Supervisor</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Accreditation Expiration</label>
                                <input type="date" class="form-control" name="accreditation_expiration" value="{{ $row->accreditation_expiration }}" required />
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary" >Save changes</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    @endforeach
    @foreach($data as $row)
        <div id="delete{{ $row->id }}" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h6 class="modal-title">Remove Record</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <form action="/instructor-courses/{{ $row->id }}" method="POST" id="needs-validation" novalidate="" enctype="multipart/form-data">
                        @csrf
                        @method('DELETE')

                        <div class="modal-body">
                            <h5>Delete This Record?</h5>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-danger">Remove</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    @endforeach

@endsection

@section('script')
    <script>
        console.log('employee users')
    </script>
    <script>
        function getSubCourse() {
            let course = $("#course_id").val();
            let sub_course = $("#sub_course").empty();

            $.ajax({
                url: '/getSubCourse/' + course,
                type: 'GET',
                success: function (response) {
                    console.log(response);
                    $.each(response, function(i, item) {
                        $('<option value="' + item.value + '">' + item.name + '</option>').
                        appendTo(sub_course);
                    });
                },
                error: function (response) {

                }
            });
        }
    </script>
    <script>
        $("#summernote-1").summernote({
            height: 150
        });
    </script>
@endsection
