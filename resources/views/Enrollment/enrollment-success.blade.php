@extends('layout.app')

@section('content')

    <main class="main">
        <!-- Breadcrumb -->

        <div class="container-fluid">

            <div class="animated fadeIn">

                <div class="row mt-lg-5">
                    <div class="col-md-12">
                        <div class="card card-accent-info">
                            <div class="card-header bg-theme">
                                <p>Congratulations!</p>
                            </div>
                            <div class="card-body">
                                <h4>You had successfully enrolled {{ $student->firstname . " " . $student->lastname }}</h4>
                                <div class="form-group row">
                                    <a href="{{ url('printTRS/'.$student->id) }}" class="btn btn-warning btn-block">Print TRS (Trainee Record Sheet)</a>
                                    <a href="{{ url('/enrollment') }}" class="btn btn-danger btn-block">Enrollment List</a>
                                    <a href="{{ url('/students') }}" class="btn btn-info btn-block">Students List</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end animated fadeIn -->
        </div>
        <!-- end container-fluid -->

    </main>

@endsection

@section('script')


@endsection
