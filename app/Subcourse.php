<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subcourse extends Model
{
    //

    protected $table = "subcourses";
    protected $guarded = [];

}
