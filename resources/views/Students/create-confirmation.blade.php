@extends('layout.app')

@section('content')
    <main class="main">
        <!-- Breadcrumb -->

        <div class="container-fluid">

            <div class="animated fadeIn">

                <div class="row mt-lg-5">
                        <div class="col-md-12">
                            <form action="/students" method="POST" enctype="multipart/form-data" style="-webkit-appearance: none">
                                @csrf
                                <div class="card card-accent-info">
                                    <div class="card-header bg-theme">
                                        <p>Verify Information</p>
                                    </div>
                                    <div class="card-body">
                                            <input type="hidden" name="verified" />
                                            <div class="form-group row offset-5">
                                                <div class="col-md-6">
                                                    <label>Date</label>
                                                    <input type="text" name="enrollment_date" class="form-control" readonly value="{{ request()->get('enrollment_date') }}" />
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Registration No</label>
                                                    <input type="text" class="form-control" name="registration_no" value="{{ request()->get('registration_no') }}" readonly />
                                                </div>
                                                <div class="col-md-6">
                                                    <label>TRS NO</label>
                                                    <input type="text" class="form-control" name="trs_no" required value="{{ request()->get('trs_no') }}" />
                                                </div>
                                            </div>
                                            <fieldset class="form-group">
                                                <legend>Personal Information</legend>
                                                <input type="hidden" name="profile" class="form-control" value="{{ request()->get('profile') }}" />
                                                <div class="form-group row">
                                                    <div class="col-md-3">
                                                        <label>Last Name</label>
                                                        <input type="text" class="form-control" name="lastname" value="{{ request()->get('lastname')  }}" placeholder="Enter Last Name" required/>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label>First Name</label>
                                                        <input type="text" class="form-control" name="firstname" value="{{ request()->get('firstname') }}" placeholder="Enter First Name" required />
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label>Middle Name</label>
                                                        <input type="text" class="form-control" name="middlename" value="{{ request()->get('middlename') }}" placeholder="Enter Middle Name" />
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label>Suffix</label>
                                                        <input type="text" class="form-control" name="suffix" value="{{ request()->get('suffix') }}" placeholder="Enter Suffix"  />
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-12">
                                                        <label>Address</label>
                                                        <textarea name="address" class="form-control" rows="4" required placeholder="Enter Address">{{ request()->get('address') }}</textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-3">
                                                        <label>Gender</label>
                                                        <select name="gender" class="form-control select2" data-plugin="select2" style="width: 100%" required>
                                                            <option value="Male" {{ request()->get('gender') === "Male"? "Selected": "" }}>Male</option>
                                                            <option value="Female" {{ request()->get('gender') === "Female"? "Selected": "" }}>Female</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label>Date of Birth</label>
                                                        <input type="date" class="form-control" name="date_of_birth" value="{{ request()->get('date_of_birth') }}" required />
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label>Place of Birth</label>
                                                        <input type="text" class="form-control" name="place_of_birth" placeholder="Enter Place of Birth" value="{{ request()->get('place_of_birth') }}" required />
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label>Contact Number</label>
                                                        <input type="text" class="form-control" name="contact_number" placeholder="09xxxxxxxxx" required value="{{ request()->get('contact_number') }}" />
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <label>Rank</label>
                                                        <select name="rank_id" data-placeholder="Select Rank" class="form-control select2" data-plugin="select2" style="width: 100%;" readonly="">
                                                            @foreach($ranks as $rank)
                                                                <option value="{{ $rank->id }} {{ request()->get('rank_id') === $rank->id? "Selected": "" }}">{{ $rank->rank }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>SRN</label>
                                                        <input type="number" class="form-control" name="srn" maxlength="9" minlength="1" placeholder="Enter your SRN" value="{{ request()->get('srn') }}" required />
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>Endorser</label>
                                                        <select name="endorser_id" data-placeholder="Select Endorser" class="form-control select2" data-plugin="select2" style="width: 100%;" required>
                                                            <option value="NONE">NONE</option>
                                                            @foreach($endorsers as $endorser)
                                                                <option value="{{ $endorser->id }} {{ request()->get('endorser_id') === $endorser->id ? "Selected": "" }}">{{ $endorser->endorser_name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <fieldset class="form-group">
                                                <legend>In Case of Emergency</legend>
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <label>Contact Name</label>
                                                        <input type="text" class="form-control" name="contact_emergency_name" value="{{ request()->get('contact_emergency_name') }}" placeholder="Enter the name of your emergency contact" />
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>Contact Number</label>
                                                        <input type="text" class="form-control" name="contact_emergency_number" value="{{ request()->get('contact_emergency_number') }}" placeholder="Enter Emergency Contact Number" />
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>Relationship</label>
                                                        <input type="text" class="form-control" name="contact_emergency_relationship" value="{{ request()->get('contact_emergency_relationship') }}" placeholder="Enter Relationship to Contact" />

                                                    </div>
                                                </div>
                                            </fieldset>

                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label>All the above information are correct?</label>
                                            <button type="submit" class="btn btn-success btn-block">Yes</button>
                                            <button type="button" class="btn btn-danger btn-block" onclick="window.history.back()">No</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                </div>
            </div>
            <!-- end animated fadeIn -->
        </div>
        <!-- end container-fluid -->

    </main>

    <div id="capture" class="modal fade" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title">Capture</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card">
                        <div class="card-header">
                            <p class="text-theme">Camera Feed</p>
                        </div>
                        <div class="card-body">
                            <center>
                                <div id="my_camera"></div>
                            </center>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" onclick="take_snapshot()">Capture</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

@endsection

@section('script')

    <script src="{{ asset('assets/js/webcam.min.js') }}"></script>

    <!-- Configure a few settings and attach camera -->
    {{--    <script language="JavaScript">--}}
    {{--        Webcam.set({--}}
    {{--            width: 390,--}}
    {{--            height: 390,--}}
    {{--            image_format: 'jpeg',--}}
    {{--            jpeg_quality: 90--}}
    {{--        });--}}

    {{--        Webcam.attach( '#my_camera' );--}}

    {{--        function take_snapshot() {--}}
    {{--            Webcam.snap( function(data_uri) {--}}
    {{--                $(".image-tag").val(data_uri);--}}
    {{--                document.getElementById('results').innerHTML = '<img src="'+data_uri+'"/>';--}}
    {{--            } );--}}
    {{--        }--}}
    {{--    </script>--}}

    <script>

            // window.open('http://127.0.0.1/students', 'popUpWindow','height=400, width=650, left=300, top=100, resizable=yes, scrollbars=yes, toolbar=yes, menubar=no, location=no, directories=no, status=yes');
    </script>
@endsection
