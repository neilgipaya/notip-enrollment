@extends('layout.app')

@section('content')
    @inject('instructor', 'App\Http\Controllers\InstructorController')
    @inject('courseController', 'App\Http\Controllers\CoursesController')

    <main class="main">
        <!-- Breadcrumb -->
        <ol class="breadcrumb bc-colored bg-theme" id="breadcrumb">
            <li class="breadcrumb-item ">
                <a href="">Schedule</a>
            </li>
            <li class="breadcrumb-item">
                <a href="#"> Manage Schedule </a>
            </li>
        </ol>

        <div class="container-fluid">

            <div class="animated fadeIn">
                <div class="row">

                    <div class="col-md-12">
                        <div class="card card-accent-theme">

                            <div class="card-body">
                                @can('create-schedules')
                                    <div align="right" >
                                        <a href="{{ url('schedule/create') }}" class="btn btn-primary" >Create </a>
                                    </div>
                                @endcan
                                <h4 class="text-theme">Schedule List

                                </h4>
                                <br />
                                <table class="display table table-hover table-striped dataTable" data-plugin="datatable" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th>Course</th>
                                        <th>Sub Course</th>
                                        <th>Class Number</th>
                                        <th>Schedule Date</th>
                                        <th>Duration</th>
                                        <th>Assessor </th>
                                        <th>Supervisor</th>
                                        <th>Enrolled Students</th>
                                        <th>Created Date</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($data as $row)
                                        <tr>
                                            <td>{{ $row->course }}</td>
                                            <td>{{ $courseController::subCourse($row->schedule_sub_course_id) }}</td>
                                            <td>{{ $row->schedule_course_id }}</td>
                                            <td>{{ $row->schedule_from . "-" . $row->schedule_to }}</td>
                                            <td>{{ $row->schedule_duration }}</td>
                                            <td>{{ $instructor::getAssessor($row->schedule_assessor_id) }}</td>
                                            <td>{{ $instructor::getSuperVisor($row->schedule_supervisor_id) }}</td>
                                            <td>
                                                @if($instructor::countEnrollees($row->schedule_course_id) > 0)
                                                    <span class="badge badge-info text-white">{{ $instructor::countEnrollees($row->schedule_course_id) }}</span>
                                                @else
                                                    <span class="badge badge-danger text-white">{{ $instructor::countEnrollees($row->schedule_course_id) }}</span>
                                                @endif
                                            </td>
                                            <td>
                                                {{ date('F d, Y', strtotime($row->created_at)) }}
                                            </td>
                                            <td class="text-nowrap" style="margin: 0">
                                                <a class="btn btn-primary btn-sm m-0" title="Print Attendance Sheet" href="{{ url('printAttendance' . "/". $row->id) }}">
                                                    <i class="fa fa-print"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- end card-body -->
                        </div>
                        <!-- end card -->
                    </div>
                    <!-- end col -->

                </div>
                <!-- end row -->
            </div>
            <!-- end animated fadeIn -->
        </div>
        <!-- end container-fluid -->
    </main>
    <!-- end main -->


@endsection

@section('script')

@endsection
