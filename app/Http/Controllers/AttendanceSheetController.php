<?php

namespace App\Http\Controllers;

use App\Rooms;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use setasign\Fpdi\Fpdi;

class AttendanceSheetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $data = DB::table('courses')
            ->join('schedules', 'schedules.course_id', '=', 'courses.id')
            ->whereNull('courses.deleted_at')
            ->get();

        $rooms = Rooms::all();

        return view('Attendance.attendance-sheet', compact('data', 'rooms'));

    }

    public function printAttendance($schedule_id){

        $schedule = DB::table('schedules')
            ->join('courses', 'courses.id', '=', 'schedules.course_id')
            ->where('schedules.id', $schedule_id)
            ->first();

        $instructor = DB::table('instructor_courses')
            ->join('instructors', 'instructors.id', '=', 'instructor_courses.instructor_id')
            ->where('instructor_courses.course_id', $schedule->course_id)
            ->get();


        $pdf = new Fpdi();

        $file = public_path('attendance.pdf');


        $pageCount = $pdf->setSourceFile($file);
// iterate through all pages
        $tpl = $pdf->importPage(1);
        $pdf->AddPage('L', 'Legal');

        $pdf->useTemplate($tpl);

        $pdf->SetFont('Arial');
        $pdf->SetFontSize('10'); // set font size

        //x = left right
        //y == taas baba

        $pdf->SetXY(30, 30); // set the position of the box
        $pdf->Cell(0, 10, $schedule->course, 'C');

        $pdf->SetXY(105, 30.5); // set the position of the box
        $pdf->Cell(0, 10, $schedule->schedule_course_id, 'C');



        return $pdf->Output();

    }
}
