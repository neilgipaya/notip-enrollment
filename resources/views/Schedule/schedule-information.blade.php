@extends('layout.app')

@section('content')
    @inject('instructor', 'App\Http\Controllers\InstructorController')
    @inject('classSchedule', 'App\Http\Controllers\ScheduleController')
    @inject('courseController', 'App\Http\Controllers\CoursesController')

    <main class="main">
        <!-- Breadcrumb -->

        <div class="container-fluid">

            <div class="animated fadeIn">
                <div class="row mt-lg-5">
                    <div class="col-md-3">
                        <div class="card card-accent-theme">
                            <div class="card-header">
                                <h6 class="text-theme">Schedule Information</h6>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h6>{{ $schedule->course }}</h6>
                                    </div>
                                </div>
                                <table class="table table-striped m-md-b-0">
                                    <tbody>
                                    <tr>
                                        <th scope="row">Duration</th>
                                        <td class="text-right">{{ $schedule->course_duration }}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Slots</th>
                                        <td class="text-right">{{ $schedule->slots }}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Assessor</th>
                                        <td class="text-right">{{ $instructor::getAssessor($schedule->schedule_assessor_id) }}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Supervisor</th>
                                        <td class="text-right">{{ $instructor::getSuperVisor($schedule->schedule_supervisor_id)}}</td>
                                    </tr>
                                    </tbody>
                                </table>

                                <div class="form-group">
                                    <button class="btn btn-block btn-info" data-toggle="modal" title="Update Schedule" data-target="#updateSchedule{{ $schedule->id }}">Update</button>
                                </div>
                            </div>
                            <!-- end card-body -->
                        </div>
                        <!-- end card -->
                    </div>
                    <div class="col-md-9">
                        <div class="card card-accent-theme">
                            <div class="card-body">
                                <h4 class="text-theme">Schedule Information</h4>
                                <ul class="nav nav-tabs custom-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#info" role="tab" aria-controls="info" aria-selected="true">
                                            <i class="fa fa-address-book"></i> Class Schedules</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#enrolled" role="tab" aria-controls="info" aria-selected="true">
                                            <i class="fa fa-user-circle-o"></i> Enrolled Students</a>
                                    </li>
                                    @can('schedules-history')
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#history" role="tab" aria-controls="info" aria-selected="true">
                                            <i class="fa fa-user-circle-o"></i> Schedule History</a>
                                    </li>
                                    @endcan
                                </ul>

                                <div class="tab-content">
                                    <div class="tab-pane active" id="info" role="tabpanel">
                                        <div class="row">
                                            @include('Schedule.class-schedule')
                                        </div>


                                    </div>
                                    <div class="tab-pane" id="enrolled" role="tabpanel">
                                        <div class="row">
                                            @include('Schedule.enrolled')
                                        </div>
                                    </div>

                                    <div class="tab-pane" id="history" role="tabpanel">
                                        <div class="row">
                                            @include('Schedule.history')
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- end-card-body -->
                        </div>
                        <!-- end card -->
                    </div>
                </div>
            </div>
            <!-- end animated fadeIn -->
        </div>
        <!-- end container-fluid -->
    </main>


    <!-- modal includes -->

    @foreach($class_schedule as $row)
        <div id="edit{{ $row->id }}" class="modal fade" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h6 class="modal-title">Edit Record</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <form action="/class-schedule/{{ $row->id }}" method="POST" id="needs-validation" novalidate="" enctype="multipart/form-data">
                        @csrf
                        @method('PATCH')

                        <div class="modal-body">
                            <div class="form-group">
                                <label>Sub Course</label>
                                <select name="schedule_course_id" class="form-control select2" data-plugin="select2" style="width: 100%;">
                                    @foreach($subcourses as $subcourse)
                                        <option value="{{ $subcourse->id }}">{{ $subcourse->sub_course }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Instructors</label>
                                <select name="instructor" class="form-control select2"  data-plugin="select2" style="width: 100%;">
                                    @foreach($instructors as $instructor)
                                        <option value="{{ $instructor->id }}" {{ $instructor->id === $row->instructor ? "selected" : "" }}>{{ $instructor->firstname . " " . $instructor->lastname }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Schedule Type</label>
                                <select name="schedule_type" class="form-control select2" data-plugin="select2" style="width: 100%;">
                                    <option value="Class">Class</option>
                                    <option value="Assessment">Assessment</option>
                                    <option value="Demo">Demo</option>
                                    <option value="Practical Assessment">Practical Assessment</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Time From</label>
                                <input type="time" class="form-control" name="time_from" value="{{ $row->time_from }}" required />
                            </div>
                            <div class="form-group">
                                <label>Time To</label>
                                <input type="time" class="form-control" name="time_to" value="{{ $row->time_to }}" required />
                            </div>
                            <div class="form-group">
                                <label>Room</label>
                                <select name="room_id" class="form-control select2"  data-plugin="select2" style="width: 100%;">
                                    @foreach($rooms as $room)
                                        <option value="{{ $room->id }}" {{ $room->id === $row->room_id ? "selected" : "" }}>{{ $room->room_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <input type="hidden" class="form-control" name="duration" placeholder="Class Duration" value="{{ $row->duration }}" />

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary" >Save changes</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    @endforeach

    @foreach($schedule_list as $row)
        <div id="updateSchedule{{ $row->id }}" class="modal fade" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h6 class="modal-title">Edit Record</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <form action="/schedule/{{ $row->id }}" method="POST" id="needs-validation" novalidate="" enctype="multipart/form-data">
                        @csrf
                        @method('PATCH')

                        <div class="modal-body">
                            <div class="form-group">
                                <label>Assessor</label>
                                <select class="form-control select2" data-plugin="select2" name="schedule_assessor_id" style="width: 100%">
                                    @foreach($assessors as $assessor)
                                        <option value="{{ $assessor->id }}">{{ $assessor->lastname . " " . $assessor->firstname }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Supervisor</label>
                                <select class="form-control select2" data-plugin="select2" name="schedule_supervisor_id" style="width: 100%">
                                    @foreach($supervisors as $supervisor)
                                        <option value="{{ $supervisor->id }}">{{ $supervisor->lastname . " " . $supervisor->firstname }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary" >Save changes</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    @endforeach

    @foreach($enrolled_students as $row)
        <div id="encodeGrade{{ $row->id }}" class="modal fade" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h6 class="modal-title">Encode Grade</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <form action="/enrollment/{{ $row->id }}" method="POST" id="needs-validation" novalidate="" enctype="multipart/form-data">
                        @csrf
                        @method('PATCH')

                        <div class="modal-body">
                           <div class="form-group">
                               <label>Grade</label>
                               <input type="text" class="form-control" name="grade" value="{{ $row->grade }}" />
                           </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary" >Save changes</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    @endforeach
@endsection

@section('script')


@endsection
