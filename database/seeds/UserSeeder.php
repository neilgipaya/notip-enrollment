<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        User::create([
            'firstname' => 'Chael Klieftrich Navkiel',
            'middlename' => 'Flores',
            'lastname' => 'Gipaya',
            'email' => 'neilgipaya@gmail.com',
            'username' => 'khoviera',
            'password' => Hash::make('iceangel1011')
        ]);
    }
}
