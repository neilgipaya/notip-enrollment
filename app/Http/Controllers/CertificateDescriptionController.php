<?php

namespace App\Http\Controllers;

use App\CertificateDescription;
use App\Courses;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;

class CertificateDescriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $data = DB::table('courses')
            ->join('certificate_descriptions', 'courses.id', '=', 'certificate_descriptions.course_id')
            ->whereNull('courses.deleted_at')
            ->get();


        $courses = Courses::all()->sortBy('course');


        return view('Certificate.certificate-setup', compact('data', 'courses'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        CertificateDescription::create($request->all());
        $notification = array(
            'message' => 'Certificate Description Created Successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CertificateDescription  $certificateDescription
     * @return \Illuminate\Http\Response
     */
    public function show(CertificateDescription $certificateDescription)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CertificateDescription  $certificateDescription
     * @return \Illuminate\Http\Response
     */
    public function edit(CertificateDescription $certificateDescription)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CertificateDescription  $certificateDescription
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        CertificateDescription::find($id)->update($request->all());
        $notification = array(
            'message' => 'Certificate Description Updated Successfully',
            'alert-type' => 'info'
        );
        return redirect()->back()->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CertificateDescription  $certificateDescription
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        CertificateDescription::find($id)->delete();
        $notification = array(
            'message' => 'Certificate Description Deleted Successfully',
            'alert-type' => 'warning'
        );
        return redirect()->back()->with($notification);
    }
}
