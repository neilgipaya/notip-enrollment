<?php

namespace App\Http\Controllers;

use App\Courses;
use App\Endorser;
use App\Enrollment;
use App\Ranks;
use App\Students;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class StudentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $data = Students::whereNull('reservation_no')->get();

        return view('Students.students', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $students = Students::all()->sortByDesc('id')->first();
        if (!empty($students)){
            $reg_no = $students->id + 1;
//            dd($reg_no, $students->registration_no);
            $registration_no = sprintf('%05d', $reg_no);
        } else {
            $registration_no = sprintf('%05d', 1);;
        }

        $ranks = Ranks::all()->sortByDesc('rank');
        $endorsers = Endorser::all();

        return view('Students.create-students', compact('registration_no', 'ranks', 'endorsers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $request->validate([
            'srn' => 'unique:students'
        ]);

        if ($request->post('source') === "Reservation"){
            $request->merge([
                'confirmed'=>'yes'
            ]);

            Students::create($request->all());

            $notification = array(
                'message' => 'Student Has been Reserved Successfully',
                'alert-type' => 'info'
            );

            return redirect()->back()->with($notification);
        }

        if ($request->has('verified')){
            $request->merge([
                'confirmed'=>'yes'
            ]);

            $student = Students::create($request->except('verified'));

            return redirect()->to('students/' . $student->id);
        }

        $endorsers = Endorser::all();

        $ranks = Ranks::all()->sortByDesc('rank');



        return view('Students.create-confirmation', compact('ranks', 'endorsers'))->with($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

        $student = Students::findOrFail($id);

        $ranks = Ranks::all();

        $courses = DB::table('courses')->join('schedules', 'schedules.course_id', '=', 'courses.id')->get();

        $enrollments = Enrollment::all()->where('student_id', $id);

        $endorsers = Endorser::all();


        return view('Students.student-information', compact('student', 'ranks', 'courses', 'enrollments', 'endorsers'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Students  $students
     * @return \Illuminate\Http\Response
     */
    public function edit(Students $students)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        if ($request->has('from_reservation')){
            Students::find($id)->update($request->except('from_reservation'));

            return redirect()->to('students/' . $id);
        }

       Students::find($id)->update($request->all());
        $notification = array(
            'message' => 'Student Updated Successfully',
            'alert-type' => 'info'
        );

        return redirect()->back()->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        Students::find($id)->delete();
        $notification = array(
            'message' => 'Student Deleted Successfully',
            'alert-type' => 'warning'
        );

        return redirect()->back()->with($notification);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function reservation(){
        $students = Students::all()->sortByDesc('id')->first();
        if (!empty($students)){
            $reg_no = $students->id + 1;
            $registration_no = sprintf('%05d', $reg_no);
        } else {
            $registration_no = sprintf('%05d', 1);;
        }

        $ranks = Ranks::all()->sortByDesc('rank');
        $endorsers = Endorser::all()->sortByDesc('endorser_name');

        return view('Students.reservation', compact('registration_no', 'ranks', 'endorsers'));
    }

    public function reservedStudents(){
        $data = Students::whereNotNull('reservation_no')->get();

        return view('Students.reserved-students', compact('data'));

    }

}
