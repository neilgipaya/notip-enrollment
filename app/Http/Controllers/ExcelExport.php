<?php

namespace App\Http\Controllers;

use App\Students;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class ExcelExport extends Controller
{
    //
    /**
     * @var GeneralUtil
     */
    private $generalUtil;

    /**
     * ExcelExport constructor.
     * @param GeneralUtil $generalUtil
     */
    public function __construct(GeneralUtil $generalUtil)
    {
        $this->generalUtil = $generalUtil;
    }

    public function testExport($file_name){
        Excel::load(public_path('tcroa/'. $file_name . ".xlsx"), function($reader)
        {
            $reader->sheet('Sheet1',function($sheet) {


                $sheet->cell('A5', function($cell) {
                    $cell->setValue('CLASS NO');
                });

                $sheet->cell('A7', function($cell) {
                    $cell->setValue('DURATION');
                });


                $students = Students::all();

                $count = 11;


                foreach ($students as $row){

                    $a = 'A'.$count;
                    $e = 'E'.$count;
                    $f = 'F'.$count;
                    $g = 'G'.$count;

                    $sheet->cell($a, function($cell) use ($row) {
                        $cell->setValue($row->lastname . " " . $row->firstname);
                    });

                    $sheet->cell($e, function($cell) use ($row) {
                        $cell->setValue($row->date_of_birth);
                    });

                    $sheet->cell($f, function($cell) use ($row) {
                        $cell->setValue($row->place_of_birth);
                    });

                    $sheet->cell($g, function($cell) use ($row) {
                        $cell->setValue($row->rank);
                    });

                    $count = $count += 1;


                }




            });
        })->export('xlsx');
    }

    public function export(Request $request){
        Excel::load(public_path('tcroa/'. $request->post('format') . ".xlsx"), function($reader) use ($request)
        {
            $reader->sheet('Sheet1',function($sheet) use ($request) {


                $sheet->cell('A5', function($cell) use ($request){
                    $cell->setValue($request->post('course_code'));
                });

                $sheet->cell('A7', function($cell) use ($request) {
                    $cell->setValue($request->post('schedule_duration'));
                });


                $students = DB::table('enrollments')
                    ->join('students', 'students.id', '=', 'enrollments.student_id')
                    ->where('enrollments.course_code', $request->post('course_code'))
                    ->get();

                $count = 11;


                foreach ($students as $row){

                    $a = 'A'.$count;
                    $e = 'E'.$count;
                    $f = 'F'.$count;
                    $g = 'G'.$count;

                    $sheet->cell($a, function($cell) use ($row) {
                        $cell->setValue($row->lastname . " " . $row->firstname);
                    });

                    $sheet->cell($e, function($cell) use ($row) {
                        $cell->setValue($row->date_of_birth);
                    });

                    $sheet->cell($f, function($cell) use ($row) {
                        $cell->setValue($row->place_of_birth);
                    });

                    $sheet->cell($g, function($cell) use ($row) {
                        $cell->setValue($this->generalUtil->getRankById($row->rank_id)->rank);
                    });

                    $count = $count += 1;


                }

            });
        })->export('xlsx');

    }


}
