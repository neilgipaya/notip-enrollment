@extends('layout.app')

@section('content')
    @inject('classSchedule', 'App\Http\Controllers\ScheduleController')

    <main class="main">
        <!-- Breadcrumb -->

        <div class="container-fluid">

            <div class="animated fadeIn">

                <div class="row mt-lg-5 offset-2">
                    <div class="col-md-10">
                        <div class="card card-accent-info">
                            <div class="card-header bg-theme">
                                <p>{{ $course_schedule->schedule_course_id }}</p>
                            </div>
                            <div class="card-body">
                                <form action="/enrollment" method="POST" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                                    <input type="hidden" value="{{ $course->id }}" name="enrollment_course_id" />
                                    <input type="hidden" value="{{ $student_id }}" name="student_id" />
                                    <input type="hidden" value="{{ $course_schedule->schedule_course_id }}" name="course_code" />
                                    <input type="hidden" value="{{ $seat_no }}" name="seat_no" />
                                    <input type="hidden" value="{{ $student->firstname . " " . $student->lastname }}" name="student_name" />
                                    <fieldset class="form-group">
                                        <legend>Enrollment Information</legend>
                                        <div class="form-group row">
                                            <div class="col-md-8 float-left">
                                                <p>
                                                    <strong>Student Name: </strong> {{ $student->firstname . " " . $student->lastname . " " }} <br />
                                                    <strong>Course: </strong> {{ $course->course }} <br />
                                                    <strong>Course Expiration: </strong> {{ date('F d, Y', strtotime($course->course_expiration)) }} <br />
                                                    <strong>Your Seat: </strong> {{ $seat_no }} <br />
                                                    <strong>Maximum Seat: </strong> {{ 24 }}
                                                </p>
                                            </div>
                                            <div class="col-md-4 float-right">
                                                <label>Type</label>
                                                <select name="enrollment_type" class="form-control select2" data-plugin="select2" style="width: 100%">
                                                    <option value="Permanent">Permanent</option>
                                                    <option value="Pencil Book">Pencil Book</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-12">
                                                <table class="display table table-hover table-striped">
                                                    <thead>
                                                    <tr>
                                                        <th>Date</th>
                                                        <th>Schedule Type</th>
                                                        <th>Time From </th>
                                                        <th>Time To</th>
                                                        <th>Instructor </th>
                                                        <th>Room</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($class_schedule as $row)
                                                        @if(\Carbon\Carbon::parse($row->date)->dayOfWeek == \Carbon\Carbon::SUNDAY)
                                                            <tr>
                                                                <td colspan="6">
                                                                    <center>SUNDAY</center>
                                                                </td>
                                                            </tr>
                                                        @else
                                                            <tr>
                                                                <td>{{ date('F d, Y', strtotime($row->date)) }}</td>
                                                                <td>
                                                                    {{ $row->schedule_type }}
                                                                </td>
                                                                <td>
                                                                    @if(isset($row->time_from))
                                                                        {{ \Carbon\Carbon::parse($row->time_from)->format('g:i A') }}
                                                                    @endif
                                                                </td>
                                                                <td>
                                                                    @if(isset($row->time_to))
                                                                        {{ \Carbon\Carbon::parse($row->time_to)->format('g:i A') }}
                                                                    @endif
                                                                </td>
                                                                <td>
                                                                    @if(isset($row->instructor))
                                                                        {{ $classSchedule::getInstructor($row->instructor) }}
                                                                    @endif
                                                                </td>
                                                                <td>
                                                                    @if(isset($row->room_id))
                                                                        {{ $classSchedule::getRoom($row->room_id) }}
                                                                    @endif
                                                                </td>
                                                            </tr>

                                                        @endif
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-block btn-success"><i class="fa fa-save"></i> Proceed Enrollment</button>
                                        <a href="{{ url('students/'. $student_id) }}" class="btn btn-block btn-danger"><i class="fa fa-undo"></i> Go Back</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end animated fadeIn -->
        </div>
        <!-- end container-fluid -->

    </main>

@endsection

@section('script')

    <script>
        function  courseChange() {
            let course = $("#course_id").val();
            let supervisor = $("#supervisor").empty();
            let assessor = $("#assessor").empty();

            $.get('generateCourseID', function (response) {

            });

            $.ajax({
                url: '/getAssessor/' + course,
                type: 'GET',
                success: function (response) {
                    console.log(response);
                    $.each(response, function(i, item) {
                        $('<option value="' + item.value + '">' + item.name + '</option>').
                        appendTo(assessor);
                    });
                },
                error: function (response) {

                }
            });

            $.ajax({
                url: '/getSupervisor/' + course,
                type: 'GET',
                success: function (response) {
                    console.log(response);
                    $.each(response, function(i, item) {
                        $('<option value="' + item.value + '">' + item.name + '</option>').
                        appendTo(supervisor);
                    });
                },
                error: function (response) {

                }
            });
        }
    </script>

@endsection
