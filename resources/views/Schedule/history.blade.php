<ul>
    @foreach($schedule_history as $history)
        <li>
            {{ $history->action }} on {{ date('F d, Y h:i:a', strtotime($history->date_time)) }}
            <br />
            <span class="fa fa-clock-o"> {{ \Carbon\Carbon::parse($history->date_time)->diffForHumans() }}</span>
        </li>
    @endforeach
</ul>
