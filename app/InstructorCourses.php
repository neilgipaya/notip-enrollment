<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class InstructorCourses extends Model
{

    //

    protected  $guarded = [];

    /*** @param $id
     * @return \Illuminate\Support\Collection
     */
    public function get_instructor_by_id($id){

        return DB::table('courses')
            ->join('instructor_courses', 'instructor_courses.course_id', '=', 'courses.id')
            ->where('instructor_courses.instructor_id', $id)
            ->get();
    }
}
