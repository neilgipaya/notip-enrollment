@extends('layout.app')

@section('content')
    <main class="main">
        <!-- Breadcrumb -->

        <div class="container-fluid">

            <div class="animated fadeIn">

                <div class="row mt-lg-5 offset-2">
                    <div class="col-md-10">
                        <div class="card card-accent-info">
                            <div class="card-header bg-theme">
                                <p>Create New Schedule</p>
                            </div>
                            <div class="card-body">
                                <form action="/schedule" method="POST" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">


                                    <fieldset class="form-group">
                                        <legend>Schedule Information</legend>
{{--                                        <div class="form-group">--}}
{{--                                            <label>School Year</label>--}}
{{--                                            <input type="text" class="form-control" value="{{ date('Y') }}" readonly />--}}
{{--                                        </div>--}}
                                        <div class="form-group row">
                                            <div class="col-md-12">
                                                <label>Course</label>
                                                <select name="course_id" class="form-control select2" data-plugin="select2" data-placeholder="Select Courses" id="course_id" onchange="courseChange()" style="width: 100%" required>
                                                    <option value="" selected>Select Course</option>
                                                    @foreach($courses as $course)
                                                        <option value="{{ $course->id }}">{{ $course->course }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
{{--                                        <div class="form-group row">--}}
{{--                                            <div class="col-md-12">--}}
{{--                                                <label>Sub Course</label>--}}
{{--                                                <select name="schedule_sub_course_id" class="form-control select2" data-plugin="select2" data-placeholder="Select Courses" id="sub_course" style="width: 100%" >--}}
{{--                                                    <option value="" selected>Select Sub Course</option>--}}

{{--                                                </select>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <label>From</label>
                                                <input type="date" class="form-control" name="schedule_from" required />
                                            </div>
                                            <div class="col-md-6">
                                                <label>To</label>
                                                <input type="date" class="form-control" name="schedule_to" required />
                                            </div>
                                        </div>
{{--                                        <div class="form-group row">--}}
{{--                                            <div class="col-md-6">--}}
{{--                                                <label>Time From</label>--}}
{{--                                                <input type="time" class="form-control" name="time_from" />--}}
{{--                                            </div>--}}
{{--                                            <div class="col-md-6">--}}
{{--                                                <label>Time To</label>--}}
{{--                                                <input type="time" class="form-control" name="time_to" />--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="form-group row">--}}
{{--                                            <div class="col-md-12">--}}
{{--                                                <label>Room</label>--}}
{{--                                                <select class="form-control select2" style="width: 100%;" data-plugin="select2" name="room_id" required>--}}
{{--                                                    @foreach($rooms as $room)--}}
{{--                                                        <option value="{{ $room->id }}">{{ $room->room_name }}</option>--}}
{{--                                                    @endforeach--}}
{{--                                                </select>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="form-group row">--}}
{{--                                            <div class="col-md-12">--}}
{{--                                                <label>Instructor</label>--}}
{{--                                                <select class="form-control" name="instructor_id" id="instructor" required>--}}

{{--                                                </select>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="form-group row">--}}
{{--                                            <div class="col-md-12">--}}
{{--                                                <label>Supervisor</label>--}}
{{--                                                <select name="schedule_supervisor_id" class="form-control select2" data-plugin="select2" data-placeholder="Select Supervisor" style="width: 100%" id="supervisor" >--}}

{{--                                                </select>--}}
{{--                                            </div>--}}
{{--                                            <div class="col-md-12">--}}
{{--                                                <label>Assessor</label>--}}
{{--                                                <select name="schedule_assessor_id" class="form-control select2" data-plugin="select2" data-placeholder="Select Assessor" style="width: 100%" id="assessor">--}}

{{--                                                </select>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
                                    </fieldset>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-block btn-success"><i class="fa fa-save"></i> Save Record</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end animated fadeIn -->
        </div>
        <!-- end container-fluid -->

    </main>

@endsection

@section('script')

    <script>
        function  courseChange() {
            let course = $("#course_id").val();
            let supervisor = $("#supervisor").empty();
            let assessor = $("#assessor").empty();
            let instructor = $("#instructor").empty();
            let sub_course = $("#sub_course").empty();

            $.ajax({
                url: '/getSubCourse/' + course,
                type: 'GET',
                success: function (response) {
                    console.log(response);
                    $.each(response, function(i, item) {
                        $('<option value="' + item.value + '">' + item.name + '</option>').
                        appendTo(sub_course);
                    });
                },
                error: function (response) {

                }
            });

            $.ajax({
                url: '/getAssessor/' + course,
                type: 'GET',
                success: function (response) {
                    console.log(response);
                    $.each(response, function(i, item) {
                        $('<option value="' + item.value + '">' + item.name + '</option>').
                        appendTo(assessor);
                    });
                },
                error: function (response) {

                }
            });

            $.ajax({
                url: '/getSupervisor/' + course,
                type: 'GET',
                success: function (response) {
                    console.log(response);
                    $.each(response, function(i, item) {
                        $('<option value="' + item.value + '">' + item.name + '</option>').
                        appendTo(supervisor);
                    });
                },
                error: function (response) {

                }
            });

            $.ajax({
                url: '/getInstructor/' + course,
                type: 'GET',
                success: function (response) {
                    console.log(response);
                    $.each(response, function(i, item) {
                        $('<option value="' + item.value + '">' + item.name + '</option>').
                        appendTo(instructor);
                    });
                },
                error: function (response) {

                }
            });
        }
    </script>

@endsection
