<?php

namespace App\Http\Controllers;

use App\Endorser;
use Illuminate\Http\Request;

class EndorserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = Endorser::all()->sortBy('endorser_name');

        return view('Settings.endorsers', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        Endorser::create($request->all());
        $notification = array(
            'message' => 'Endorser Created Successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Endorser  $endorser
     * @return \Illuminate\Http\Response
     */
    public function show(Endorser $endorser)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Endorser  $endorser
     * @return \Illuminate\Http\Response
     */
    public function edit(Endorser $endorser)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        Endorser::find($id)->update($request->all());
        $notification = array(
            'message' => 'Endorser Updated Successfully',
            'alert-type' => 'info'
        );
        return redirect()->back()->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        Endorser::find($id)->delete();
        $notification = array(
            'message' => 'Endorser Deleted Successfully',
            'alert-type' => 'warning'
        );
        return redirect()->back()->with($notification);
    }
}
