@extends('layout.app')

@section('styles')
    <style>
        .pic {
            border-radius: 50%;
            background-color: #cccccc;
            width: 2em;
            height: 2em;
            display: flex;
            align-items: center;
            justify-content: center;
            font-size: 120px;
            color: #ffffff;
            box-shadow: 0px 0px 6px rgba(20, 20, 20, 0.6);
        }

        .name {
            color: #222222;
        }
    </style>
@endsection

@section('content')
    <main class="main">
        <!-- Breadcrumb -->

        <div class="container-fluid">

            <div class="animated fadeIn">

                <div class="row mt-lg-5">
                    <div class="row">
                        <div class="col-md-10 offset-3">
                            <div class="card card-accent-info">
                                <div class="card-header bg-theme">
                                    <div class="float-left">
                                        <p>Enroll New Student</p>
                                    </div>
                                    <div class="float-right">
                                        <input type=button class="form-control" data-target="#capture" data-toggle="modal" value="Open Camera">
                                    </div>
                                </div>
                                <div class="card-body">
                                    <form action="/students" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <input type="hidden" name="source" class="form-control" value="Walk-in" />
                                        <div class="form-group row offset-5">
                                            <div class="col-md-6">
                                                <label>Date</label>
                                                <input type="text" name="enrollment_date" class="form-control" readonly value="{{ date('Y-m-d') }}" />
                                            </div>
                                            <div class="col-md-6">
                                                <label>Student No</label>
                                                <input type="text" class="form-control" name="registration_no" value="{{ date('Y-m-').$registration_no }}" readonly />
                                            </div>
                                            <div class="col-md-6">
                                                <label>TRS NO</label>
                                                <input type="text" class="form-control" name="trs_no" required />
                                            </div>
{{--                                            <div class="col-md-6">--}}
{{--                                                <label>Registration No</label>--}}
{{--                                                <input type="text" class="form-control" name="registration_no" required />--}}
{{--                                            </div>--}}
                                        </div>
                                        <fieldset class="form-group">
                                            <legend>Personal Information</legend>
                                            <input type="hidden" name="profile" class="image-tag">
                                            <div class="form-group row ">
                                                <div class="col-md-6">
                                                    <div id="results">

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-md-3">
                                                    <label>Last Name</label>
                                                    <input type="text" class="form-control" name="lastname" value="{{ old('lastname') }}" placeholder="Enter Last Name" required/>
                                                </div>
                                                <div class="col-md-3">
                                                    <label>First Name</label>
                                                    <input type="text" class="form-control" name="firstname" value="{{ old('firstname') }}" placeholder="Enter First Name" required />
                                                </div>
                                                <div class="col-md-3">
                                                    <label>Middle Name</label>
                                                    <input type="text" class="form-control" name="middlename" value="{{ old('middlename') }}" placeholder="Enter Middle Name" />
                                                </div>
                                                <div class="col-md-3">
                                                    <label>Suffix</label>
                                                    <input type="text" class="form-control" name="suffix" value="{{ old('suffix') }}" placeholder="Enter Suffix"  />
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-md-12">
                                                    <label>Address</label>
                                                    <textarea name="address" class="form-control" rows="4" required placeholder="Enter Address">{{ old('address') }}</textarea>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-md-3">
                                                    <label>Gender</label>
                                                    <select name="gender" class="form-control select2" data-plugin="select2" style="width: 100%" required>
                                                        <option value="Male">Male</option>
                                                        <option value="Female">Female</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-3">
                                                    <label>Date of Birth</label>
                                                    <input type="date" class="form-control" name="date_of_birth" value="{{ old('date_of_birth') }}" required />
                                                </div>
                                                <div class="col-md-3">
                                                    <label>Place of Birth</label>
                                                    <input type="text" class="form-control" name="place_of_birth" placeholder="Enter Place of Birth" value="{{ old('place_of_birth') }}" required />
                                                </div>
                                                <div class="col-md-3">
                                                    <label>Contact Number</label>
                                                    <input type="text" class="maxlength-input form-control" data-plugin="maxlength" data-placement="bottom-right-inside" maxlength="13"  name="contact_number" placeholder="09xxxxxxxxx" required value="{{ old('contact_number') }}" />
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    <label>Rank</label>
                                                    <select name="rank_id" data-placeholder="Select Rank" class="form-control select2" data-plugin="select2" style="width: 100%;" required>
                                                        @foreach($ranks as $rank)
                                                            <option value="{{ $rank->id }}">{{ $rank->rank }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>SRN</label>
                                                    <input type="text" class="maxlength-input form-control" name="srn" data-plugin="maxlength" data-placement="bottom-right-inside" maxlength="10" placeholder="Enter your SRN" value="{{ old('srn') }}">
                                                </div>
                                                <div class="col-md-4">
                                                    <label>Endorser</label>
                                                    <select name="endorser_id" data-placeholder="Select Endorser" class="form-control select2" data-plugin="select2" style="width: 100%;" required>
                                                        <option value="NONE">NONE</option>
                                                        @foreach($endorsers as $endorser)
                                                            <option value="{{ $endorser->id }}">{{ $endorser->endorser_name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset class="form-group">
                                            <legend>In Case of Emergency</legend>
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    <label>Contact Name</label>
                                                    <input type="text" class="form-control" name="contact_emergency_name" value="{{ old('contact_emergency_name') }}" placeholder="Enter the name of your emergency contact" />
                                                </div>
                                                <div class="col-md-4">
                                                    <label>Contact Number</label>
                                                    <input type="text" class="maxlength-input form-control" data-plugin="maxlength" data-placement="bottom-right-inside" maxlength="13" name="contact_emergency_number" value="{{ old('contact_emergency_number') }}" placeholder="Enter Emergency Contact Number" />
                                                </div>
                                                <div class="col-md-4">
                                                    <label>Relationship</label>
                                                    <input type="text" class="form-control" name="contact_emergency_relationship" value="{{ old('contact_emergency_relationship') }}" placeholder="Enter Relationship to Contact" />

                                                </div>
                                            </div>
                                        </fieldset>

                                        <div class="form-group">
                                            <button type="submit" class="btn btn-block btn-success"><i class="fa fa-save"></i> Save Record</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end animated fadeIn -->
        </div>
        <!-- end container-fluid -->

    </main>

    <div id="capture" class="modal fade" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title">Capture</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                    <div class="modal-body">
                        <div class="card">
                            <div class="card-header">
                                <p class="text-theme">Camera Feed</p>
                            </div>
                            <div class="card-body">
                               <center>
                                   <div id="my_camera"></div>
                               </center>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" onclick="take_snapshot()">Capture</button>
                    </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

@endsection

@section('script')

    <script src="{{ asset('assets/js/webcam.min.js') }}"></script>
    
    <script>
        $(function(){
            $("#srn").maxlength({
                threshold: 10
             });
        });

    </script>

    <!-- Configure a few settings and attach camera -->
    <script language="JavaScript">
        Webcam.set({
            width: 390,
            height: 390,
            image_format: 'jpeg',
            jpeg_quality: 90
        });

        Webcam.attach( '#my_camera' );

        function take_snapshot() {
            Webcam.snap( function(data_uri) {
                $(".image-tag").val(data_uri);
                document.getElementById('results').innerHTML = '<img src="'+data_uri+'" class="pic"/>';
            } );
        }
    </script>
@endsection
