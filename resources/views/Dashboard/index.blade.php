@extends('layout.app')

@section('content')

    <main class="main">
        <!-- Breadcrumb -->
        <ol class="breadcrumb bc-colored bg-theme" id="breadcrumb">
            <li class="breadcrumb-item ">
                <a href="">Home</a>
            </li>
            <li class="breadcrumb-item">
                <a href="#">Dashboards</a>
            </li>
        </ol>

        <div class="container-fluid">

            <div class="animated fadeIn">

                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-pm-summary bg-theme">
                            <div class="card-body">

                                <div class="clearfix">
                                    <div class="float-left">
                                        <div class="h3 text-white">
                                            <strong>NOTIP Dashboard</strong>
                                        </div>
                                    </div>
                                    <div class="float-right">
                                        <a href="{{ url('/students/create') }}" class="btn btn-dark">Enroll New Student</a>
                                    </div>
                                </div>
                                <!-- end clearfix -->

                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="card-body">
                                            <div class="widget-pm-summary">
                                                <i class="fa fa-user-circle"></i>
                                                <div class="widget-text">
                                                    <div class="h2 text-white">{{ $totalEmployee }}</div>
                                                    <small class="text-white">User Resources</small>
                                                </div>
                                                <!-- end widget-text -->
                                            </div>
                                            <!-- end widget-pm-simmary -->
                                        </div>
                                        <!-- end card-body -->
                                    </div>
                                    <!-- end inside-col -->

                                    <div class="col-md-3">
                                        <div class="card-body">
                                            <div class="widget-pm-summary">
                                                <i class="fa fa-pencil-square-o"></i>
                                                <div class="widget-text">
                                                    <div class="h2 text-white">{{ $totalCourses }}</div>
                                                    <small class="text-white">Courses</small>
                                                </div>
                                                <!-- end widget-text -->
                                            </div>
                                            <!-- end widget-pm-simmary -->
                                        </div>
                                        <!-- end card-body -->
                                    </div>
                                    <!-- end inside-col -->

                                    <div class="col-md-3">
                                        <div class="card-body">
                                            <div class="widget-pm-summary">
                                                <i class="fa fa-building-o"></i>
                                                <div class="widget-text">
                                                    <div class="h2 text-white">{{ $totalStudents }}</div>
                                                    <small class="text-white">Students</small>
                                                </div>
                                                <!-- end widget-text -->
                                            </div>
                                            <!-- end widget-pm-simmary -->
                                        </div>
                                        <!-- end card-body -->
                                    </div>
                                    <!-- end inside-col -->

                                    <div class="col-md-3">
                                        <div class="card-body">
                                            <div class="widget-pm-summary">
                                                <i class="fa fa-clock-o"></i>
                                                <div class="widget-text">
                                                    <div class="h2 text-white">{{ $totalSchedules }}</div>
                                                    <small class="text-white">Schedules</small>
                                                </div>
                                                <!-- end widget-text -->
                                            </div>
                                            <!-- end widget-pm-simmary -->
                                        </div>
                                        <!-- end card-body -->
                                    </div>
                                    <!-- end inside-col -->

                                </div>
                                <!-- end inside row -->

                            </div>
                            <!-- end card-body -->
                        </div>
                        <!-- end card -->
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="card card-accent-danger">
                                    <div class="card-body">
                                        <div class="clearfix">
                                            <div class="float-right">
                                                <div class="h2 text-danger">{{ $reservedStudents }}</div>
                                            </div>
                                        </div>
                                        <div class="float-left">
                                            <div class="h3 ">
                                                <strong>Reserved</strong>
                                            </div>
                                            <div class="h6 text-danger"> Students </div>
                                        </div>
                                    </div>
                                    <!-- end card-body -->
                                </div>
                                <!-- end card -->
                            </div>
                            <!-- end inside col -->
                            <div class="col-md-6">
                                <div class="card card-accent-success">
                                    <div class="card-body">
                                        <div class="clearfix">
                                            <div class="float-right">
                                                <div class="h2 text-success">{{ $enrolledStudents }}</div>
                                            </div>
                                        </div>
                                        <div class="float-left">
                                            <div class="h3 ">
                                                <strong>Enrolled</strong>
                                            </div>
                                            <div class="h6 text-success">Students </div>
                                        </div>
                                    </div>
                                    <!-- end card-body -->
                                </div>
                                <!-- end card -->
                            </div>
                            <!-- end inside col -->
                        </div>
                        <!-- end inside row -->

                        <div class="row">
                            <div class="col-md-6">
                                <div class="card card-accent-primary">
                                    <div class="card-body">
                                        <div class="clearfix">
                                            <div class="float-right">
                                                <div class="h2 text-primary">{{ $pendingPayment }}</div>
                                            </div>
                                        </div>
                                        <div class="float-left">
                                            <div class="h3 ">
                                                <strong>Pending</strong>
                                            </div>
                                            <div class="h6 text-primary"> Payments </div>
                                        </div>
                                    </div>
                                    <!-- end card-body -->
                                </div>
                                <!-- end card -->
                            </div>
                            <!-- end inside col -->
                            <div class="col-md-6">
                                <div class="card card-accent-info">
                                    <div class="card-body ">
                                        <div class="clearfix">
                                            <div class="float-right">
                                                <div class="h2 text-info">{{ $downpayments }}</div>
                                            </div>
                                        </div>
                                        <div class="float-left">
                                            <div class="h3 ">
                                                <strong>Downpayments</strong>
                                            </div>
                                            <div class="h6 text-info"> Payments </div>
                                        </div>
                                    </div>
                                    <!-- end card-body -->
                                </div>
                                <!-- end card -->
                            </div>
                            <!-- end inside col -->

                        </div>
                        <!-- end inside row -->
                    </div>
                    <!-- end col -->

                    <div class="col-md-6">
                        <div class="card card-accent-theme">
                            <div class="card-body">
                                <div class="h5 ">
                                    <strong>Expiring Accreditation</strong>
                                </div>
                                <small class="text-theme">FOR UPCOMING 6 MONTHS</small>
                                <table class="display table table-hover table-striped dataTable" data-plugin="datatable" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Course</th>
                                        <th>Accreditation Expiration</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($expiring_accreditation as $row)
                                        @if(isset($row['date_difference']))
{{--                                            @if($row['date_difference'] >= 6)--}}
                                                <tr>
                                                    <td>{{ $row['name'] }}</td>
                                                    <td>{{ $row['course'] }}</td>
                                                    <td>{{ $row['accreditation_expiration'] }}</td>
                                                </tr>
{{--                                            @endif--}}
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        <!-- end card-body -->
                        </div>
                    <!-- end card -->
                    </div>
                <!-- end col -->
                </div>
                <!-- end row -->

            {{--<div class="row">--}}

            {{--<div class="col-md-6">--}}
            {{--<div class="card  card-accent-theme">--}}
            {{--<div class="message-widget">--}}
            {{--<h1> Logs--}}
            {{--<i class="fa fa-comments-o float-right"></i>--}}
            {{--</h1>--}}
            {{--<ul id="messageList">--}}
            {{--@foreach($logs as $lg)--}}
            {{--<li class="clearfix">--}}
            {{--<a href="#" class="dropdown-item">--}}
            {{--<div class="message-box ">--}}
            {{--<div class="u-text float-left">--}}
            {{--<div class="u-name">--}}
            {{--<strong>{{ $lg->username }}</strong>--}}
            {{--</div>--}}
            {{--<p class="text-muted">{{ $lg->content }}</p>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--<small class="float-right">{{ $lg->created_at }}</small>--}}
            {{--</a>--}}
            {{--</li>--}}
            {{--@endforeach--}}

            {{--</ul>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--<!-- end card -->--}}
            {{--</div>--}}
            {{--<!-- end col -->--}}

            {{--<div class="col-md-6">--}}
            {{--<div class="row">--}}
            {{--<div class="col-md-12">--}}
            {{--<div class="card card-accent-theme">--}}
            {{--<div class="card-body">--}}
            {{--<div class="h5 ">--}}
            {{--<strong>Files Per Department Analysis</strong>--}}
            {{--</div>--}}
            {{--<small class="text-theme">BASED ON THIS YEAR</small>--}}
            {{--<div id="data1"></div>--}}
            {{--</div>--}}
            {{--<!-- end card-body -->--}}
            {{--</div>--}}
            {{--<!-- end card -->--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--<!-- end inside row -->--}}
            {{--</div>--}}
            {{--<!-- end col -->--}}

            {{--</div>--}}
            <!-- end row -->

            </div>
            <!-- end animated fadeIn -->
        </div>
        <!-- end container-fluid -->

    </main>
    <!-- end main -->

@endsection

@section('script')
    <script src="https://js.pusher.com/4.4/pusher.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
    <script>
        console.log('Dashboard');
    </script>
@endsection
