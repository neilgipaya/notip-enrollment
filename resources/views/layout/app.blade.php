<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <meta name="description" content="Petropal Dashboard" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <title>NOTIP</title>

    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('assets') }}/img/favicon/apple-touch-icon.png" />
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('assets') }}/img/favicon/favicon-32x32.png" />
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets') }}/img/favicon/favicon-16x16.png" />
    <link rel="manifest" href="{{ asset('assets') }}/img/favicon/manifest.json" />
    <link rel="mask-icon" href="{{ asset('assets') }}/img/favicon/safari-pinned-tab.svg" color="#5bbad5" />
    <meta name="theme-color" content="#ffffff" />

    <!-- fonts -->
    <link rel="stylesheet" href="{{ asset('assets') }}/fonts/md-fonts/css/materialdesignicons.min.css" />
    <link rel="stylesheet" href="{{ asset('assets') }}/fonts/font-awesome-4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="{{ asset('assets') }}/fonts/simple-line-icons/css/simple-line-icons.css">
    <!-- animate css -->
    <link rel="stylesheet" href="{{ asset('assets') }}/libs/animate.css/animate.min.css" />

    <!-- data table -->
    <link rel="stylesheet" href="{{ asset('assets') }}/libs/tables-datatables/dist/datatables.min.css" />
    <!-- jquery-loading -->
    <link rel="stylesheet" href="{{ asset('assets') }}/libs/jquery-loading/dist/jquery.loading.min.css" />
    <!-- octadmin main style -->
    <link rel="stylesheet" href="{{ asset('assets') }}/libs/editor-summernote/dist/summernote-lite.css" />

    <link id="pageStyle" rel="stylesheet" href="{{ asset('assets') }}/css/style-xing.css" />
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <script src="https://js.pusher.com/4.4/pusher.min.js"></script>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">

    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <![endif]-->

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    @yield('styles')

</head>

<body class="app sidebar-fixed aside-menu-off-canvas aside-menu-hidden header-fixed">
<header class="app-header navbar">
    <div class="hamburger hamburger--arrowalt-r navbar-toggler mobile-sidebar-toggler d-lg-none mr-auto">
        <div class="hamburger-box">
            <div class="hamburger-inner"></div>
        </div>
    </div>
    <!-- end hamburger -->
    <a class="navbar-brand" href="">
        NOTIP
    </a>

    <div class="hamburger hamburger--arrowalt-r navbar-toggler sidebar-toggler d-md-down-none mr-auto">
        <div class="hamburger-box">
            <div class="hamburger-inner"></div>
        </div>
    </div>
    <!-- end hamburger -->


    <ul class="nav navbar-nav">
        {{--<li class="nav-item ">--}}
            {{--<a class="nav-link" href="#" data-toggle="dropdown">--}}
                {{--<i class="mdi mdi-bell-ring-outline"></i>--}}
                {{--<span class="notification hertbit"></span>--}}
            {{--</a>--}}
            {{--<div class="dropdown-menu dropdown-menu-right notification-list animated flipInY nicescroll-box">--}}

                {{--<div class="dropdown-header">--}}
                    {{--<strong>Notification</strong>--}}
                    {{--<span class="badge badge-pill badge-theme pull-right"> new 5</span>--}}
                {{--</div>--}}
                {{--<!--end dropdown-header -->--}}

                {{--<div class="wrap">--}}

                    {{--<a href="#" class="dropdown-item">--}}
                        {{--<div class="message-box">--}}
                            {{--<div class="u-img">--}}
                                {{--<img src="{{ asset('assets') }}/img/products/product-1.jpg" alt="user" />--}}
                            {{--</div>--}}
                            {{--<!-- end u-img -->--}}
                            {{--<div class="u-text">--}}
                                {{--<div class="u-name">--}}
                                    {{--<strong>A New Order has Been Placed </strong>--}}
                                {{--</div>--}}
                                {{--<small>2 minuts ago</small>--}}
                            {{--</div>--}}
                            {{--<!-- end u-text -->--}}
                        {{--</div>--}}
                        {{--<!-- end message-box -->--}}
                    {{--</a>--}}
                    {{--<!-- end dropdown-item -->--}}

                {{--</div>--}}
                {{--<!-- end wrap -->--}}

                {{--<div class="dropdown-footer ">--}}
                    {{--<a href="./tables-data-table.html">--}}
                        {{--<strong>See all messages (150) </strong>--}}
                    {{--</a>--}}
                {{--</div>--}}
                {{--<!-- end dropdown-footer -->--}}
            {{--</div>--}}
            {{--<!-- end notification-list -->--}}

        {{--</li>--}}
        <!-- end nav-item -->

        <!-- end nav-item -->



        <!-- end navitem -->

        <li class="nav-item dropdown">
            <a class="btn btn-round btn-theme btn-sm" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">

                    <span class="">
                        {{ Auth::user()->name }}
                        <i class="fa fa-arrow-down"></i>
                    </span>
            </a>
            <div class="dropdown-menu dropdown-menu-right user-menu animated flipInY ">
                <div class="wrap">
                    <div class="dw-user-box">
                        <div class="u-text">
                            <h5>{{ Auth::user()->name }}</h5>
                            <p class="text-muted">{{ Auth::user()->email }}</p>
                            <a href="{{ url('profile') }}" class="btn btn-round btn-theme btn-sm">View Profile</a>
                        </div>
                    </div>
                    <!-- end dw-user-box -->

                    <div class="divider"></div>

                    <a class="dropdown-item" href="{{ url('logout') }}">
                        <i class="fa fa-lock"></i> Logout</a>
                </div>
                <!-- end wrap -->
            </div>
            <!-- end dropdown-menu -->
        </li>
        <!-- end nav-item -->


    </ul>

</header>
<!-- end header -->

<div class="app-body">
    <div class="sidebar" id="sidebar">
        <nav class="sidebar-nav" id="sidebar-nav-scroller">

            <ul class="nav">
                @can('admin-dashboard')
                <li class="nav-item nav-dropdown">
                    <a class="nav-link" href="{{ url('dashboard') }}">
                        <i class="mdi mdi-gauge"></i> Dashboard </a>
                </li>
                @endcan
                @can('user-profile')
                <li class="nav-item nav-dropdown">
                    <a class="nav-link" href="{{ url('profile') }}">
                        <i class="mdi mdi-account"></i> Account </a>
                </li>
                @endcan
                @can('students-list')
                <li class="nav-item nav-dropdown">
                    <a class="nav-link nav-dropdown-toggle" href="#">
                        <i class="mdi mdi-account-box"></i> Students</a>
                    <ul class="nav-dropdown-items">
                        @can('enroll-students')
                        <li class="nav-item">
                            <a class="nav-link" href="{{ URL::route('students.create') }}"> Enroll New Student</a>
                        </li>
                        @endcan
                        @can('reserve-students')
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('reservedStudents') }}"> Enroll from Reserved Student</a>
                        </li>
                        @endcan
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('students') }}">
                                Manage Students
                            </a>
                        </li>
                    </ul>
                </li>
                @endcan
                @can('schedules-list')
                <li class="nav-item nav-dropdown">
                    <a class="nav-link" href="{{ url('schedule') }}">
                        <i class="mdi mdi-clock"></i> Schedules & Enrollment </a>
                </li>
                @endcan
                @can('reserve-students')
                <li class="nav-item nav-dropdown">
                    <a class="nav-link" href="{{ url('reservation') }}">
                        <i class="mdi mdi-clock-alert"></i> Student Reservation </a>
                </li>
                @endcan
                @can('payment-list')
                <li class="nav-item nav-dropdown">
                    <a class="nav-link nav-dropdown-toggle" href="#">
                        <i class="mdi mdi-account-box"></i> Payment</a>
                    <ul class="nav-dropdown-items">
                        @can('waiting-list')
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('waitingList') }}"> Waiting List</a>
                        </li>
                        @endcan
                        @can('pending-payments-list')
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('pendingPayment') }}"> Pending Payment</a>
                        </li>
                        @endcan
                        @can('downpayments-list')
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('paymentCompletion') }}"> Downpayment</a>
                        </li>
                        @endcan
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('paymentPaid') }}"> Paid</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('payment') }}">Payment List</a>
                        </li>
                    </ul>
                </li>
                @endcan
                @can('certificate-listing')
                <li class="nav-item nav-dropdown">
                    <a class="nav-link" href="{{ url('certificates') }}">
                        <i class="mdi mdi-certificate"></i> Certification </a>
                </li>
                <li class="nav-item nav-dropdown">
                    <a class="nav-link" href="{{ url('certificate-setup') }}">
                        <i class="mdi mdi-certificate"></i> Certificate Configuration </a>
                </li>
                @endcan
                @can('releasing')
                <li class="nav-item nav-dropdown">
                    <a class="nav-link" href="{{ url('releasing') }}">
                        <i class="mdi mdi-keyboard-return"></i> Releasing </a>
                </li>
                @endcan
                @can('endorser-list')
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('endorser') }}">
                        <i class="mdi mdi-hospital-building"></i>
                        Endorser / Agency</a>
                </li>
                @endcan
                @can('attendance')
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('attendance-sheet') }}">
                        <i class="mdi mdi-calendar-clock"></i>
                        Attendance Sheet</a>
                </li>
                @endcan
                @can('configuration-access')
                <li class="nav-item nav-dropdown">
                    <a class="nav-link nav-dropdown-toggle" href="#">
                        <i class="mdi mdi-console"></i> Training & Scheduling</a>
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('ranks') }}"> Ranks</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('courses') }}"> Courses</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('rooms') }}"> Rooms</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('instructor') }}"> Instructor</a>
                        </li>
                    </ul>
                </li>
                @endcan
                @can('user-list')
                <li class="nav-item nav-dropdown">
                    <a class="nav-link nav-dropdown-toggle" href="#">
                        <i class="mdi mdi-settings"></i> Settings</a>
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('users') }}"> System Users</a>
                        </li>
                        @can('roles-list')
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('roles') }}"> Roles & Permissions</a>
                        </li>
                        @endcan
                    </ul>
                </li>
                @endcan

            </ul>

        </nav>

    </div>
    <!-- end sidebar -->

    @yield('content')
    <!-- end main -->

</div>
<!-- end app-body -->

<footer class="app-footer">
    <a href="#" class="text-theme">NOTIP</a> &copy; {{ date('Y') }}.
</footer>

<!-- Bootstrap and necessary plugins -->
<script src="{{ asset('assets') }}/libs/jquery/dist/jquery.min.js"></script>
<script src="{{ asset('assets') }}/libs/nicescroll/jquery.nicescroll.min.js"></script>
<script src="{{ asset('assets') }}/libs/popper.js/dist/umd/popper.min.js"></script>
<script src="{{ asset('assets') }}/libs/bootstrap/bootstrap.min.js"></script>
<script src="{{ asset('assets') }}/libs/PACE/pace.min.js"></script>
<script src="{{ asset('assets') }}/libs/chart.js/dist/Chart.min.js"></script>
<!--toastr -->
<script src="{{ asset('assets') }}/libs/toastr/toastr.min.js"></script>

<!-- jquery-loading -->
<script src="{{ asset('assets') }}/libs/jquery-loading/dist/jquery.loading.min.js"></script>
<!--datatables -->
<script src="{{ asset('assets') }}/libs/tables-datatables/dist/datatables.min.js"></script>

<!-- dropify -->
<script src="{{ asset('assets') }}/libs/dropify/dist/js/dropify.min.js"></script>
<!--select 2 -->
<script src="{{ asset('assets') }}/libs/select2/dist/js/select2.min.js"></script>

<!-- bootstrap tags input -->
<script src="{{ asset('assets') }}/libs/bootstrap-tagsinput/dist/tagsinput.js"></script>

<!-- multiselect -->
<script src="{{ asset('assets') }}/libs/multiselect/js/jquery.multi-select.js"></script>

<!-- typeahead -->
<script src="{{ asset('assets') }}/libs/typeahead/typeahead.js"></script>

<!-- max-length -->
<script src="{{ asset('assets') }}/libs/bootstrap-maxlength/src/bootstrap-maxlength.js"></script>

<!-- bootstrap touchspin -->
<script src="{{ asset('assets') }}/libs/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"></script>

<!-- clockpicker -->
<script src="{{ asset('assets') }}/libs/clockpicker/dist/bootstrap-clockpicker.min.js"></script>

<!-- timepicker -->
<script src="{{ asset('assets') }}/libs/timepicker/dist/jquery.timepicker.min.js"></script>

<!-- datepicker -->
<script src="{{ asset('assets') }}/libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>

<!-- slider-range -->
<script src="{{ asset('assets') }}/libs/sliderrange/dist/jquery-asRange.min.js"></script>

<!-- jquery-strength -->
<script src="{{ asset('assets') }}/libs/jquery-strength/dist/password_strength.js"></script>
<script src="{{ asset('assets') }}/libs/jquery-strength/dist/jquery-strength.js"></script>

<!-- jquery-knob -->
<script src="{{ asset('assets') }}/libs/jquery-knob/dist/jquery.knob.min.js"></script>

<!-- jquery-labelauty -->
<script src="{{ asset('assets') }}/libs/jquery-labelauty/source/jquery-labelauty.js"></script>
<!--summernote -->
<script src="{{ asset('assets') }}/libs/editor-summernote/dist/summernote-lite.js"></script>

<!-- octadmin Main Script -->
<script src="{{ asset('assets') }}/js/app.js"></script>

<!-- summernote -example -->
<script src="{{ asset('assets') }}/js/editor-summernote-example.js"></script>



<!-- datatable examples -->
<script src="{{ asset('assets') }}/js/table-datatable-example.js"></script>
<!-- dropify examples -->
<script src="{{ asset('assets') }}/js/dropify-examples.js"></script>
<script src="{{ asset('assets') }}/js/form-plugins-example.js"></script>

<script>
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "1000",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
        @if(Session::has('message'))
    var type = "{{ Session::get('alert-type', 'info') }}";
    switch(type){
        case 'info':
            toastr.info("{{ Session::get('message') }}");
            break;

        case 'warning':
            toastr.warning("{{ Session::get('message') }}");
            break;

        case 'success':
            toastr.success("{{ Session::get('message') }}");
            break;

        case 'error':
            toastr.error("{{ Session::get('message') }}");
            break;
    }
    @endif
</script>
@if ($errors->any())
    @foreach ($errors->all() as $error)
        <script>
            toastr.warning('{{ $error }}');
        </script>
    @endforeach
@endif

<script>
    $("#BasicDataTable").DataTable({
        responsive: true,
        ordering: false,
    });
</script>

@yield('script')
</body>

</html>
