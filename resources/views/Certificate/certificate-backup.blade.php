@extends('layout.app')

@section('content')
    @inject('instructor', 'App\Http\Controllers\InstructorController')
    @inject('courseController', 'App\Http\Controllers\CoursesController')

    <main class="main">
        <!-- Breadcrumb -->
        <ol class="breadcrumb bc-colored bg-theme" id="breadcrumb">
            <li class="breadcrumb-item ">
                <a href="">Certificate</a>
            </li>
            <li class="breadcrumb-item">
                <a href="#"> Manage Certificate </a>
            </li>
        </ol>

        <div class="container-fluid">

            <div class="animated fadeIn">
                <div class="row">

                    <div class="col-md-12">
                        <div class="card card-accent-theme">

                            <div class="card-body">
                                <h4 class="text-theme">Students List

                                </h4>
                                <br />
                                <table class="display table table-hover table-striped dataTable" data-plugin="datatable" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Course</th>
                                        <th>Sub Course</th>
                                        <th>Class Number</th>
                                        <th>Schedule Date</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($data as $row)
                                        <tr>
                                            <td>{{ $row->lastname . " " . $row->firstname }}</td>
                                            <td>{{ $row->course }}</td>
                                            <td>{{ $courseController::subCourse($row->schedule_sub_course_id) }}</td>
                                            <td>{{ $row->schedule_course_id }}</td>
                                            <td>{{ $row->schedule_from . "-" . $row->schedule_to }}</td>
                                            <td class="text-nowrap" style="margin: 0">
                                                <a class="btn btn-info btn-sm m-0" title="Print Certificate" data-toggle="modal" data-target="#print{{ $row->id }}">
                                                    <i class="fa fa-print"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- end card-body -->
                        </div>
                        <!-- end card -->
                    </div>
                    <!-- end col -->

                </div>
                <!-- end row -->
            </div>
            <!-- end animated fadeIn -->
        </div>
        <!-- end container-fluid -->
    </main>
    <!-- end main -->

    @foreach($data as $row)
        <div id="print{{ $row->id }}" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h6 class="modal-title">Print Certificate</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <form action="certificatePrinting" method="POST" id="needs-validation" novalidate="" enctype="multipart/form-data">
                        @csrf

                        <input type="text" class="form-control" name="student_id" value="{{ $row->student_id }}" />
                        <input type="text" class="form-control" name="enrollment_course_id" value="{{ $row->course_code }}" />
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Registration Number</label>
                                <input type="text" class="form-control" name="registration_number" required />
                            </div>
                            <div class="form-group">
                                <label>Select Format</label>
                                <select class="form-control select2" data-plugin="select2" name="format" style="width: 100%">
                                    @foreach($format as $row)
                                        <option value="{{ $row->id }}">{{ $row->certificate_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-danger">Print</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    @endforeach


@endsection

@section('script')

@endsection
