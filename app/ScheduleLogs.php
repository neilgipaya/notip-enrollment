<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScheduleLogs extends Model
{
    //

    protected $table = "schedule_logs";

    protected $guarded = [];
}
