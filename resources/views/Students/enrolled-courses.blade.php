<table class="display table table-bordered" style="width: 100%; margin-top: 0">
    <thead>
    <tr>
        <th>Course Code</th>
        <th>Enrollment Type</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    @foreach($enrollments as $row)
        <tr>
            <td>{{ $row->course_code }}</td>
            <td>{{ $row->enrollment_type}}</td>
            <td class="text-nowrap" style="margin: 0">
                <a class="btn btn-danger btn-sm m-0" href="#" title="Remove from Enrollment" data-toggle="modal" data-target="#removeEnrollment{{ $row->id }}">
                    <i class="fa fa-undo"></i>
                </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
