<?php

namespace App\Http\Controllers;

use App\Enrollment;
use App\Payables;
use App\Students;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PayablesController extends Controller
{
    /**
     * @var GeneralUtil
     */
    private $generalUtil;

    /**
     * PayablesController constructor.
     * @param GeneralUtil $generalUtil
     */
    public function __construct(GeneralUtil $generalUtil)
    {
        $this->generalUtil = $generalUtil;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = Payables::all();

        return view('Payables.payables', compact('data'));
    }

    public function pendingPayment()
    {
        $data = Payables::all()->where('status', 'Pending');

        return view('Payables.payables', compact('data'));

    }

    public function paymentCompletion()
    {

        $data = Payables::all()->where('status', 'Downpayment');

        return view('Payables.payables', compact('data'));
    }

    public function waitingList()
    {
        //
        $data = Payables::all()->where('status', 'WaitingList');

        return view('Payables.payables', compact('data'));
    }

    public function paymentPaid()
    {
        $data = Payables::all()->where('status', 'Paid');

        return view('Payables.payables', compact('data'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Payables  $payables
     * @return \Illuminate\Http\Response
     */
    public function show(Payables $payables)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Payables  $payables
     * @return \Illuminate\Http\Response
     */
    public function edit(Payables $payables)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        try{

            if ($request->has('edit_payment')){
                Payables::find($id)->update($request->except('edit_payment'));

                $notification = array(
                    'message' => 'Payment Info Has been Updated',
                    'alert-type' => 'info'
                );

                return redirect()->back()->with($notification);

            }

            $balance = $request->post('balance');
            $total_payables = $request->post('total_payables');
            $amount_paid = $request->post('amount_paid');

            if ($amount_paid >= $total_payables){

                Payables::find($id)->update(
                    [
                        'balance'=>0,
                        'amount_paid'=>$amount_paid,
                        'status'=>'Paid'
                    ]
                );

                $notification = array(
                    'message' => 'Enrollment Fee has been Paid',
                    'alert-type' => 'info'
                );

                return redirect()->back()->with($notification);


            } elseif ($amount_paid < $total_payables){
                $payables = Payables::find($id);

                $new_balance = $payables->balance - $amount_paid;

                $total_amount_paid = $payables->amount_paid + $amount_paid;

                if ($total_amount_paid >= $payables->balance){
                    Payables::find($id)->update(
                        [
                            'balance'=>0,
                            'amount_paid'=>$amount_paid + $payables->amount_paid,
                            'status'=>'Paid'
                        ]
                    );

                    $notification = array(
                        'message' => 'Enrollment Fee has been Paid',
                        'alert-type' => 'info'
                    );

                    return redirect()->back()->with($notification);

                } else {
                    Payables::find($id)->update(
                        [
                            'balance'=>$new_balance,
                            'amount_paid'=>$amount_paid + $payables->amount_paid,
                            'status'=>'Downpayment'
                        ]
                    );

                    $notification = array(
                        'message' => 'Downpayment Settled',
                        'alert-type' => 'info'
                    );

                    return redirect()->back()->with($notification);
                }
            }




        } catch (\Exception $e){
            $notification = array(
                'message' => 'Fatal Error, Please Contact Administrator',
                'alert-type' => 'error'
            );

            $this->generalUtil->errorLogs("Payables", $e->getCode(), $e->getMessage());
            return redirect()->back()->with($notification);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Payables  $payables
     * @return \Illuminate\Http\Response
     */
    public function destroy(Payables $payables)
    {
        //
    }

    public static function getStudentInfo($student_id)
    {
        $student = Students::find($student_id);

        return $student->firstname . " " . $student->lastname;
    }

    public static function getEnrollmentInfo($enrollment_id){

        $enrollment = Enrollment::find($enrollment_id);

        return $enrollment->course_code;
    }
}
