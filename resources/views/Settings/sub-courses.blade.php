@extends('layout.app')

@section('content')
    <main class="main">
        <!-- Breadcrumb -->

        <div class="container-fluid">

            <div class="animated fadeIn">
                <div class="row mt-lg-5">
                    <div class="col-md-4">
                        <div class="card card-accent-theme">
                            <div class="card-header">
                                <h6 class="text-theme">Basic Information</h6>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <p>{{ $course->course }}</p>
                                    </div>
                                </div>
                                <table class="table table-striped m-md-b-0">
                                    <tbody>
                                    <tr>
                                        <th scope="row">Course Code</th>
                                        <td class="text-right">{{ $course->course_code }}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Course Expiration</th>
                                        <td class="text-right">{{ $course->course_expiration }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- end card-body -->
                        </div>
                        <!-- end card -->
                    </div>
                    <div class="col-md-8">
                        <div class="card card-accent-theme">
                            <div class="card-body">
                                <h4 class="text-theme">Sub Courses</h4>
                                <ul class="nav nav-tabs custom-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#info" role="tab" aria-controls="info" aria-selected="true">
                                            <i class="fa fa-file"></i>Course List</a>
                                    </li>
                                </ul>

                                <div class="tab-content">
                                    <div class="tab-pane active" id="info" role="tabpanel">
                                        <div class="col-md-12">
                                            <div align="right" >
                                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#create">Create </button>
                                            </div>
                                            <table class="display table table-hover table-striped dataTable" data-plugin="datatable" cellspacing="0" width="100%">
                                                <thead>
                                                <tr>
                                                    <th>Sub Course Code </th>
                                                    <th>Sub Course</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($data as $row)
                                                    <tr>
                                                        <td>{{ $row->sub_course_code }}</td>
                                                        <td>{{ $row->sub_course }}</td>
                                                        <td class="text-nowrap" style="margin: 0">

                                                            <a class="btn btn-warning btn-sm m-0" title="Edit" href="#" data-toggle="modal" data-target="#edit{{ $row->id }}">
                                                                <i class="fa fa-pencil"></i>
                                                            </a>
                                                            <a class="btn btn-danger btn-sm m-0" title="Delete" href="#" data-toggle="modal" data-target="#delete{{ $row->id }}" >
                                                                <i class="fa fa-close"></i>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- end-card-body -->
                        </div>
                        <!-- end card -->
                    </div>
                </div>
            </div>
            <!-- end animated fadeIn -->
        </div>
        <!-- end container-fluid -->
    </main>



    <div id="create" class="modal fade" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title">Create Record</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form action="/sub-courses" method="POST" id="needs-validation" novalidate="" enctype="multipart/form-data">
                    @csrf

                    <div class="modal-body">
                        <input type="hidden" name="course_id" value="{{ $course->id }}" />
                        <div class="form-group">
                            <label>Sub Course Code</label>
                            <input type="text" class="form-control" name="sub_course_code" required />
                        </div>
                        <div class="form-group">
                            <label>Sub Course</label>
                            <input type="text" class="form-control" name="sub_course" required />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save Record</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    @foreach($data as $row)
        <div id="edit{{ $row->id }}" class="modal fade" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h6 class="modal-title">Edit Record</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <form action="/sub-courses/{{ $row->id }}" method="POST" id="needs-validation" novalidate="" enctype="multipart/form-data">
                        @csrf
                        @method('PATCH')

                        <div class="modal-body">
                            <input type="hidden" name="course_id" value="{{ $course->id }}" />
                            <div class="form-group">
                                <label>Sub Course Code</label>
                                <input type="text" class="form-control" name="sub_course_code" value="{{ $row->sub_course_code }}" required />
                            </div>
                            <div class="form-group">
                                <label>Sub Course</label>
                                <input type="text" class="form-control" name="sub_course" value="{{ $row->sub_course }}" required />
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary" >Save changes</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    @endforeach
    @foreach($data as $row)
        <div id="delete{{ $row->id }}" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h6 class="modal-title">Remove Record</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <form action="/sub-courses/{{ $row->id }}" method="POST" id="needs-validation" novalidate="" enctype="multipart/form-data">
                        @csrf
                        @method('DELETE')

                        <div class="modal-body">
                            <h5>Delete This Record?</h5>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-danger">Remove</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    @endforeach

@endsection

@section('script')
    <script>
        console.log('employee users')
    </script>
    <script>
        $("#summernote-1").summernote({
            height: 150
        });
    </script>
@endsection
