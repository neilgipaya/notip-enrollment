<table class="display table table-hover table-striped dataTable" data-plugin="datatable" style="width: 100%">
    <thead>
    <tr>
        <th>Profile</th>
        <th>SRN</th>
        <th>Full Name</th>
        <th>Rank</th>
        <th>Grade</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
        @foreach($enrolled_students as $row)
            <tr>
                <td><img src="{{ $row->profile }}" class="img img-avatar" style="width: 100px" /> </td>
                <td>{{ $row->srn }}</td>
                <td>{{ $row->firstname . " " . $row->lastname  }}</td>
                <td>{{ $classSchedule::getRank($row->rank_id)}}</td>
                <td>
                    {{ $row->grade }}
                </td>
                <td>
                    <a class="btn btn-info btn-sm m-0" data-toggle="modal" data-target="#encodeGrade{{ $row->id }}" title="View More">
                        <i class="fa fa-balance-scale"></i>
                    </a>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
