<?php

namespace App\Http\Controllers;

use App\ClassSchedule;
use App\Courses;
use App\Endorser;
use App\Enrollment;
use App\Payables;
use App\Ranks;
use App\Students;
use Carbon\Traits\Date;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Exception;
use setasign\Fpdi\Fpdi;

class EnrollmentController extends Controller
{
    /**
     * @var GeneralUtil
     */
    private $generalUtil;

    /**
     * EnrollmentController constructor.
     * @param GeneralUtil $generalUtil
     */
    public function __construct(GeneralUtil $generalUtil)
    {
        $this->generalUtil = $generalUtil;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        try{

            $data = DB::table('courses')
                ->join('schedules', 'schedules.course_id', '=', 'courses.id')
                ->whereNull('courses.deleted_at')
                ->get();

            return view('Enrollment.enrollment-list', compact('data'));
        }
        catch (\Exception $e){
            $notification = array(
                'message' => 'Fatal Error, Please Contact Administrator',
                'alert-type' => 'error'
            );

            $this->generalUtil->errorLogs("Enrollment", $e->getCode(), $e->getMessage());
            return redirect()->back()->with($notification);
        }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return void
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try{
            $enrollment = Enrollment::create($request->except('student_name'));


            $this->generalUtil->generateScheduleLogs($message = $request->post('student_name') . " Has been enrolled", $enrollment->course_code);
            $this->generalUtil->studentLogs($request->post('student_id'), "You have been enrolled to a new course");

            $course = Courses::find($request->post('enrollment_course_id'));

            $this->addStudentPayable($request->post('student_id'), $enrollment->id,  $course->course_price);

            $notification = array(
                'enrollment_id' => $enrollment->id,
                'message' => 'Student Enrolled Successfully',
                'alert-type' => 'success'
            );

            return redirect('/enrollment-success/'. $enrollment->student_id)->with($notification);
        } catch (\Exception $e){
            $notification = array(
                'message' => 'Fatal Error, Please Contact Administrator',
                'alert-type' => 'error'
            );

            $this->generalUtil->errorLogs("Enrollment", $e->getCode(), $e->getMessage());
            return redirect()->back()->with($notification);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Enrollment  $enrollment
     * @return \Illuminate\Http\Response
     */
    public function show(Enrollment $enrollment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Enrollment  $enrollment
     * @return \Illuminate\Http\Response
     */
    public function edit(Enrollment $enrollment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return void
     */
    public function update(Request $request, $id)
    {
        //

        Enrollment::find($id)->update($request->all());

        $notification = array(
            'message' => 'Info Updated Successfully',
            'alert-type' => 'info'
        );



        return redirect()->back()->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Enrollment  $enrollment
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try{
            $enrollment = Enrollment::find($id)->delete();
//            DB::table('payables')
//                ->where('student_id')
            $notification = array(
                'message' => 'Enrollment Info Deleted Successfully',
                'alert-type' => 'error'
            );



            return redirect()->back()->with($notification);
        } catch (\Exception $e){
            $notification = array(
                'message' => 'Fatal Error, Please Contact Administrator',
                'alert-type' => 'error'
            );

            $this->generalUtil->errorLogs("Enrollment", $e->getCode(), $e->getMessage());
            return redirect()->back()->with($notification);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function enroll(Request $request){

        $course_code = $request->post('course_id');
        $student_id = $request->post('student_id');

        return redirect('/studentEnrollment/'. $course_code . "/" . $student_id);

    }

    /**
     * @param $course_code
     * @param $student_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function studentEnrollment($course_code, $student_id){


        try{

            $student = Students::find($student_id);

            $course_schedule = DB::table('courses')
                ->join('schedules', 'schedules.course_id', '=', 'courses.id')
                ->where('schedules.schedule_course_id', $course_code)
                ->first();

            $course = Courses::find($course_schedule->course_id);

            $seat_no = $this->getSeatNumber($course_schedule->course_id, $course_code);

            $class_schedule = ClassSchedule::all()->where('schedule_id', $course_schedule->id);

            return view('Enrollment.enrollment', compact('course', 'student_id', 'student', 'seat_no', 'class_schedule', 'course_schedule'));

        } catch (\Exception $e){

            $notification = array(
                'message' => 'Fatal Error, Please Contact Administrator',
                'alert-type' => 'error'
            );

            $this->generalUtil->errorLogs("Enrollment", $e->getCode(), $e->getMessage());
            return redirect()->back()->with($notification);
        }


    }

    public function enrollmentSuccess($student_id)
    {
        $student = Students::find($student_id);

        return view('Enrollment.enrollment-success', compact('student'));
    }


    public function printTRS($student_id){
        try{
            $student = Students::find($student_id);

            $enrollment = DB::table('enrollments')
                ->where('student_id', $student_id)
                ->first();

            $rank = Ranks::find($student->rank_id);

            $agency = Endorser::find($student->endorser_id);
            $endorser = "NONE";
            if (isset($agency)){
                $endorser = $agency->endorser_name;
            }
            $course = Courses::find($enrollment->enrollment_course_id);

            $schedule = DB::table('class_schedules')
                ->join('schedules', 'schedules.id', '=', 'class_schedules.schedule_id')
                ->join('rooms', 'rooms.id', '=', 'class_schedules.room_id')
                ->where('schedules.schedule_course_id', '=', $enrollment->course_code)
                ->get();


            $pdf = new Fpdi();

            $file = public_path('blank.pdf');


            $pageCount = $pdf->setSourceFile($file);
// iterate through all pages
            $tpl = $pdf->importPage(1);
            $pdf->AddPage();

            $pdf->useTemplate($tpl);

            $pdf->SetFont('Helvetica');

            $pdf->SetFontSize('11'); // set font size
            $pdf->SetXY(168, 4.5); // set the position of the box
            $pdf->Cell(0, 10, $enrollment->seat_no , 'C');

            $pdf->SetXY(163, 42.5); // set the position of the box
            $pdf->Cell(0, 10, $student->trs_no , 'C');

            $pdf->SetXY(18, 43.7); // set the position of the box
            $pdf->Cell(0, 10, $student->firstname . " " . $student->lastname, 'C');

            $pdf->SetXY(41, 47.8); // set the position of the box
            $pdf->Cell(0, 10, $rank->rank, 'C');
//
            $pdf->SetXY(19, 52); // set the position of the box
            $pdf->Cell(0, 10, $endorser, 'C');

            $pdf->SetXY(19, 57.5); // set the position of the box
            $pdf->Cell(0, 10, $course->course, 'C');
//
            $pdf->SetXY(19, 62.5); // set the position of the box
            $pdf->Cell(0, 10, $enrollment->course_code, 'C');
//
            $y = 80;
            foreach ($schedule as $row){
                $pdf->SetFontSize('7'); // set font size
                $pdf->SetXY(6, $y); // set the position of the box
                $pdf->Cell(0, 10, date('F d, Y', strtotime($row->date)), '', '', '', '', '');

                $pdf->SetFontSize('7'); // set font size
                $pdf->SetXY(24, $y); // set the position of the box
                $pdf->Cell(0, 10, date('h:iA', strtotime($row->time_from)) . "-" . date('h:iA', strtotime($row->time_to)), '', '', '', '', '');

                $pdf->SetFontSize('7'); // set font size
                $pdf->SetXY(49, $y); // set the position of the box
                $pdf->Cell(0, 10, $row->room_name, '', '', '', '', '');

                $y = $y + 5;

            }

            return $pdf->Output();


        } catch (Exception $e){
            dd($e->getLine() . $e->getMessage());
            $notification = array(
                'message' => 'Fatal Error, Please Contact Administrator',
                'alert-type' => 'error'
            );

            $this->generalUtil->errorLogs("Enrollment", $e->getCode(), $e->getMessage());
            return redirect()->back()->with($notification);
        }

    }

    public static function getEnrolledStudents($course_code){
        return DB::table('enrollments')
            ->where('course_code', $course_code)
            ->count();
    }

    /**
     * @param $course_id
     * @param $course_code
     * @return mixed
     */
    private function getSeatNumber($course_id, $course_code){
        $total = 24;

        $enrollment = DB::table('enrollments')->where('enrollment_course_id', $course_id)->where('course_code', $course_code)->sum('seat_no');

        $seat = min($total - $enrollment, 1);
        return $seat;
    }

    private function addStudentPayable($student_id, $enrollment_id, $amount)
    {

        Payables::create([
            'student_id' => $student_id,
            'enrollment_id' => $enrollment_id,
            'total_payables' => $amount,
            'balance' => $amount,
            'amount_paid' => 0,
            'status' => 'Pending'
        ]);


        
    }



}
