@extends('layout.app')

@section('content')
    @inject('payables', 'App\Http\Controllers\PayablesController')

    <main class="main">
        <!-- Breadcrumb -->
        <ol class="breadcrumb bc-colored bg-theme" id="breadcrumb">
            <li class="breadcrumb-item ">
                <a href="">Payment</a>
            </li>
            <li class="breadcrumb-item">
                <a href="#"> All Payables </a>
            </li>
        </ol>

        <div class="container-fluid">

            <div class="animated fadeIn">
                <div class="row">

                    <div class="col-md-12">
                        <div class="card card-accent-theme">

                            <div class="card-body">
                                <h4 class="text-theme">Payment List</h4>
                                <br />
                                <table class="display table table-hover table-striped dataTable" data-plugin="datatable" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th>Reference No</th>
                                        <th>Student</th>
                                        <th>Course Code</th>
                                        <th>Total Payables</th>
                                        <th>Balance</th>
                                        <th>Amount Paid</th>
                                        @can('payments-manage')
                                        <th>Action</th>
                                        @endcan
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($data as $row)
                                        <tr>
                                            <td>{{ $row->reference_no }}</td>
                                            <td>{{ $payables::getStudentInfo($row->student_id) }}</td>
                                            <td>{{ $payables::getEnrollmentInfo($row->enrollment_id) }}</td>
                                            <td>{{ $row->total_payables }}</td>
                                            <td>{{ $row->balance }}</td>
                                            <td>{{ $row->amount_paid }}</td>
                                            @can('payments-manage')
                                            <td class="text-nowrap" style="margin: 0">
                                                @if($row->status !== "Paid")
                                                    <a class="btn btn-danger btn-sm m-0" title="Add Payment" href="#" data-toggle="modal" data-target="#payment{{ $row->id }}" >
                                                        <i class="fa fa-usd"></i>
                                                    </a>
                                                @endif
                                                @can('payment-edit')
                                                    <a class="btn btn-warning btn-sm m-0" title="Edit Payment Info" href="#" data-toggle="modal" data-target="#paymentEdit{{ $row->id }}" >
                                                        <i class="fa fa-pencil-square-o"></i>
                                                    </a>
                                                @endcan
                                            </td>
                                            @endcan
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- end card-body -->
                        </div>
                        <!-- end card -->
                    </div>
                    <!-- end col -->

                </div>
                <!-- end row -->
            </div>
            <!-- end animated fadeIn -->
        </div>
        <!-- end container-fluid -->
    </main>
    <!-- end main -->

    @foreach($data as $row)
        <div id="payment{{ $row->id }}" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h6 class="modal-title">Add Payment</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <form action="/payment/{{ $row->id }}" method="POST" id="needs-validation" novalidate="" enctype="multipart/form-data">
                        @csrf
                        @method('PATCH')

                        <div class="modal-body">
                            <div class="form-group">
                                <p>
                                    <strong>Total Payables: </strong> {{ $row->total_payables }} <br />
                                    <strong>Balance: </strong> {{ $row->balance }} <br />
                                    <strong>Amount Paid: </strong> {{ $row->amount_paid }} <br />
                                </p>
                            </div>
                            <input type="hidden" name="balance" value="{{ $row->balance }}" />
                            <input type="hidden" name="total_payables" value="{{ $row->total_payables }}" />
                            <div class="form-group">
                                <label>Amount to Pay</label>
                                <input type="number" class="form-control" name="amount_paid" step="any" required />
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-danger">Pay</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    @endforeach

    @foreach($data as $row)
        <div id="paymentEdit{{ $row->id }}" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h6 class="modal-title">Edit Payment Details</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <form action="/payment/{{ $row->id }}" method="POST" id="needs-validation" novalidate="" enctype="multipart/form-data">
                        @csrf
                        @method('PATCH')

                        <div class="modal-body">
                            <input type="hidden" name="edit_payment" value="edit_payment" />
                            <div class="form-group">
                                <label>Total Payables</label>
                                <input type="number" class="form-control" name="total_payables" value="{{ $row->total_payables }}" readonly="" />
                            </div>
                            <div class="form-group">
                                <label>Balance</label>
                                <input type="number" class="form-control" name="balance" value="{{ $row->balance }}" />
                            </div>
                            <div class="form-group">
                                <label>Amount Paid</label>
                                <input type="number" class="form-control" name="amount_paid" value="{{ $row->amount_paid }}" />
                            </div>
                            <div class="form-group">
                                <label>Status</label>
                                <select name="status" class="form-control select2" data-plugin="select2" style="width: 100%;">
                                    <option value="Pending" {{ $row->status === "Pending" ? "Selected": "" }}>Pending</option>
                                    <option value="Downpayment" {{ $row->status === "Downpayment" ? "Selected": "" }}>Downpayment</option>
                                    <option value="Paid" {{ $row->status === "Paid" ? "Selected": "" }}>Paid</option>
                                </select>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-warning">Save Changes</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    @endforeach


@endsection

@section('script')

@endsection
