@extends('layout.app')

@section('content')
    <main class="main">
        <!-- Breadcrumb -->

        <div class="container-fluid">

            <div class="animated fadeIn">

                <div class="row mt-lg-5">
                    <div class="col-md-12">
                        <div class="card card-accent-theme">
                            <div class="card-body">
                                <h4 class="text-theme">
                                    Parking Users
                                </h4>
                                <table class="table table-hover dataTable table-striped w-full" data-plugin="dataTable" width="100%">
                                    <thead>
                                    <tr>
                                        <th>Username</th>
                                        <th>Email</th>
                                        <th>Full Name</th>
                                        <th>Gender</th>
                                        <th>Address</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @foreach($users as $row)
                                        <tr>
                                            <td>{{ $row->username }}</td>
                                            <td>{{ $row->email }}</td>
                                            <td>{{ $row->lastname .", ". $row->firstname }}</td>
                                            <td>{{ $row->gender }}</td>
                                            <td>{{ $row->address }}</td>
                                            <td>
                                                <a href="" data-target="#edit{{ $row->id }}" data-toggle="modal" class="btn
                                                        btn-xs
                                                        btn-warning" style="margin-top: -5px"title="Edit Record"><i class="icon-pencil"
                                                                                                                    style="color: black;"></i> </a>
                                                @if(Auth::user()->role == 0)
                                                    <a href="" data-target="#delete{{ $row->id }}" data-toggle="modal" class="btn
                                                            btn-xs  btn-danger" style="margin-top: -5px" title="Delete Record"><i class="icon-trash"
                                                                                                                                  style="color: white;"></i>
                                                    </a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                            </div>
                            <!-- end card-body -->
                        </div>
                        <!-- end card -->
                    </div>
                </div>
            </div>
            <!-- end animated fadeIn -->
        </div>
        <!-- end container-fluid -->

        @foreach($users as $row)
            <div id="edit{{ $row->id }}" class="modal fade" role="dialog" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h6 class="modal-title">Edit Record</h6>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <form action="/users/{{ $row->id }}" method="POST" id="needs-validation" novalidate="" enctype="multipart/form-data">
                            @csrf
                            @method('PATCH')

                            <div class="modal-body">
                                <div class="form-group">
                                    <label>Username</label>
                                    <input type="text" class="form-control" name="username" value="{{ $row->username }}" />
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" class="form-control" name="email" value="{{ $row->email }}" />
                                </div>
                                <div class="form-group">
                                    <label>First Name</label>
                                    <input type="text" class="form-control" name="firstname" value="{{ $row->firstname }}" />
                                </div>
                                <div class="form-group">
                                    <label>Middle Name</label>
                                    <input type="text" class="form-control" name="middlename" value="{{ $row->middlename }}" />
                                </div>
                                <div class="form-group">
                                    <label>Last Name</label>
                                    <input type="text" class="form-control" name="lastname" value="{{ $row->lastname }}" />
                                </div>
                                <div class="form-group">
                                    <label>Address</label>
                                    <textarea name="address" class="form-control" rows="4">{{ $row->address }}</textarea>
                                </div>
                                <div class="form-group">
                                    <label>Gender</label>
                                    <select name="gender" class="form-control">
                                        <option value="Male" {{ $row->gender == 'Male'? "Selected": "" }}>Male</option>
                                        <option value="Female"  {{ $row->gender == 'Female'? "Selected": "" }}>Female</option>
                                    </select>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary" name="edit">Save changes</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
        @endforeach
        @foreach($users as $row)
            <div id="delete{{ $row->id }}" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h6 class="modal-title">Remove Record</h6>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <form action="/users/{{ $row->id }}" method="POST" id="needs-validation" novalidate="" enctype="multipart/form-data">
                            @csrf
                            @method('DELETE')

                            <div class="modal-body">
                                <h5>Delete This Record?</h5>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-danger">Remove</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
        @endforeach
    </main>
@endsection

@section('script')
    <script>
        console.log('employee users')
    </script>
@endsection
