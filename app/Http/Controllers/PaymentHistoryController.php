<?php

namespace App\Http\Controllers;

use App\PaymentHistory;
use Illuminate\Http\Request;

class PaymentHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PaymentHistory  $paymentHisttory
     * @return \Illuminate\Http\Response
     */
    public function show(PaymentHistory $paymentHisttory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PaymentHistory  $paymentHisttory
     * @return \Illuminate\Http\Response
     */
    public function edit(PaymentHistory $paymentHisttory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PaymentHistory  $paymentHisttory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PaymentHistory $paymentHisttory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PaymentHistory  $paymentHisttory
     * @return \Illuminate\Http\Response
     */
    public function destroy(PaymentHistory $paymentHisttory)
    {
        //
    }
}
