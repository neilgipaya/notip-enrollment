<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentLogs extends Model
{
    //

    protected $table = "student_logs";

    protected $guarded = [];
}
