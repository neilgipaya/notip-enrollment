@extends('layout.app')

@section('content')
    <main class="main">
        <!-- Breadcrumb -->
        <ol class="breadcrumb bc-colored bg-theme" id="breadcrumb">
            <li class="breadcrumb-item ">
                <a href="">Settings</a>
            </li>
            <li class="breadcrumb-item">
                <a href="#"> Certificates </a>
            </li>
        </ol>

        <div class="container-fluid">

            <div class="animated fadeIn">
                <div class="row">

                    <div class="col-md-12">
                        <div class="card card-accent-theme">

                            <div class="card-body">
                                <div align="right" >
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#create">Create </button>
                                </div>
                                <h4 class="text-theme">Certificates</h4>
                                <br />
                                <table class="display table table-hover table-striped dataTable" data-plugin="datatable" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th>Course </th>
                                        <th>Certificate Name</th>
                                        <th>Certificate #</th>
                                        <th>Certificate Description</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($data as $row)
                                        <tr>
                                            <td>{{ $row->course }}</td>
                                            <td>{{ $row->certificate_name }}</td>
                                            <td>{{ $row->certificate_no }}</td>
                                            <td>{!!  $row->certificate_description !!} </td>
                                            <td class="text-nowrap" style="margin: 0">
                                                <a class="btn btn-warning btn-sm m-0" title="Edit" href="#" data-toggle="modal" data-target="#edit{{ $row->id }}">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                                <a class="btn btn-danger btn-sm m-0" title="Delete" href="#" data-toggle="modal" data-target="#delete{{ $row->id }}" >
                                                    <i class="fa fa-close"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- end card-body -->
                        </div>
                        <!-- end card -->
                    </div>
                    <!-- end col -->

                </div>
                <!-- end row -->
            </div>
            <!-- end animated fadeIn -->
        </div>
        <!-- end container-fluid -->
    </main>
    <!-- end main -->


    <div id="create" class="modal fade" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title">Create Record</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form action="/certificate-setup" method="POST" id="needs-validation" novalidate="" enctype="multipart/form-data">
                    @csrf

                    <div class="modal-body">
                        <div class="form-group">
                            <label>Course</label>
                            <select name="course_id" class="form-control select2" data-plugin="select2" required style="width: 100%">
                                @foreach($courses as $row)
                                    <option value="{{ $row->id }}">{{ $row->course }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Certificate Name</label>
                            <input type="text" class="form-control" name="certificate_name" required />
                        </div>
                        <div class="form-group">
                            <label>Certificate No</label>
                            <input type="text" class="form-control" name="certificate_no" />
                        </div>
                        <div class="form-group">
                            <label>Certificate Description</label>
                            <textarea name="certificate_description" class="form-control" rows="5"></textarea>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save Record</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    @foreach($data as $row)
        <div id="edit{{ $row->id }}" class="modal fade" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h6 class="modal-title">Edit Record</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <form action="/certificate-setup/{{ $row->id }}" method="POST" id="needs-validation" novalidate="" enctype="multipart/form-data">
                        @csrf
                        @method('PATCH')

                        <div class="modal-body">
                            <div class="form-group">
                                <label>Course</label>
                                <select name="course_id" class="form-control select2" data-plugin="select2" required style="width: 100%">
                                    @foreach($courses as $course)
                                        <option value="{{ $course->id }}" {{ $row->course_id === $course->id ? "Selected" : "" }}>{{ $course->course }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Certificate Name</label>
                                <input type="text" class="form-control" name="certificate_name" value="{{ $row->certificate_name }}" />
                            </div>
                            <div class="form-group">
                                <label>Certificate No</label>
                                <input type="text" class="form-control" name="certificate_no" value="{{ $row->certificate_no }}" />
                            </div>
                            <div class="form-group">
                                <label>Certificate Description</label>
                                <textarea name="certificate_description" class="form-control" rows="5">{!!  $row->certificate_description !!}</textarea>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary" >Save changes</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    @endforeach

    @foreach($data as $row)
        <div id="delete{{ $row->id }}" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h6 class="modal-title">Remove Record</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <form action="/certificate-setup/{{ $row->id }}" method="POST" id="needs-validation" novalidate="" enctype="multipart/form-data">
                        @csrf
                        @method('DELETE')

                        <div class="modal-body">
                            <h5>Delete This Record?</h5>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-danger">Remove</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    @endforeach


@endsection

@section('script')

    <script>
        $('.description').summernote({
            height: '150px',
        });
    </script>

@endsection
