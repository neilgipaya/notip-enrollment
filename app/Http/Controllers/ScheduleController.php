<?php

namespace App\Http\Controllers;

use App\ClassSchedule;
use App\Courses;
use App\Instructor;
use App\Ranks;
use App\Rooms;
use App\Schedule;
use App\ScheduleLogs;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ScheduleController extends Controller
{
    /**
     * @var GeneralUtil
     */
    private $generalUtil;

    /**
     * ScheduleController constructor.
     * @param GeneralUtil $generalUtil
     */
    public function __construct(GeneralUtil $generalUtil)
    {
        $this->generalUtil = $generalUtil;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        try{
            $data = DB::table('courses')
                ->join('schedules', 'schedules.course_id', '=', 'courses.id')
                ->whereNull('courses.deleted_at')
                ->get();

            $rooms = Rooms::all();

            return view('Schedule.schedule-list', compact('data', 'rooms'));

        } catch (\Exception $e){
            $notification = array(
                'message' => 'Fatal Error, Please Contact Administrator',
                'alert-type' => 'error'
            );

            $this->generalUtil->errorLogs("Schedule", $e->getCode(), $e->getMessage());
            return redirect()->back()->with($notification);
        }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        $courses = Courses::all()->sortBy('course');
        $rooms = Rooms::all();


        return view('Schedule.schedule-create', compact('courses', 'rooms'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        try{

            $course = $this->getCourseById($request->post('course_id'));
            $duration = (int) $course->course_duration;
//            $date_diff = $this->generalUtil->diffInDate(
//                [
//                    $request->post('schedule_from'),
//                    $request->post('schedule_to')
//                ]
//            );


//            if ($date_diff > $duration){
//                $notification = array(
//                    'message' => 'Duration must be less than or equal to '. $duration,
//                    'alert-type' => 'error'
//                );
//
//                return redirect()->back()->with($notification);
//            }

            $request->merge([
                'schedule_duration'=>$duration,
                'slots'=>24,
                'schedule_course_id'=>$this->generateCourseID($request->post('course_id'))
            ]);


            $schedule = Schedule::create($request->except('instructor_id', 'room_id', 'time_from', 'time_to'));

            $period = CarbonPeriod::create($request->post('schedule_from'), $request->post('schedule_to'));
            foreach ($period as $date){
                //create schedule here
                ClassSchedule::create(
                    [
                        'schedule_id' =>$schedule->id,
                        'date' => $date->format('Y-m-d'),
                        'instructor'=> $request->post('instructor_id'),
                        'room_id' => $request->post('room_id'),
                        'time_from' => $request->post('time_from'),
                        'time_to' => $request->post('time_to'),
                        'schedule_type' => 'Class',
                        'duration' => $this->generalUtil->diffInDate(
                           [
                               $request->post('schedule_from'),
                               $request->post('schedule_to')
                           ]
                        )
                    ]
                );
            }

            $this->generalUtil->generateScheduleLogs($message = $schedule->schedule_course_id . " has been created", $schedule->schedule_course_id);

            return redirect()->to('schedule/'. $schedule->id);

        } catch (\Exception $e){
            $notification = array(
                'message' => 'Fatal Error, Please Contact Administrator',
                'alert-type' => 'error'
            );

            $this->generalUtil->errorLogs("Schedule", $e->getCode(), $e->getMessage());
            return redirect()->back()->with($notification);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return void
     */
    public function show($id)
    {
        //
        // try{
            $schedule = DB::table('courses')
                ->join('schedules', 'schedules.course_id', '=', 'courses.id')
                ->whereNull('courses.deleted_at')
                ->where('schedules.id', '=', $id)
                ->first();


            $instructors = DB::table('instructors')
                ->join('instructor_courses', 'instructors.id', '=', 'instructor_courses.instructor_id')
                ->where('instructor_courses.course_id', $schedule->course_id)
                ->where('instructor_courses.position', 'Instructor')
                ->whereNull('instructor_courses.deleted_at')
                ->whereNull('instructors.deleted_at')
                ->groupBy('instructors.id') //add a line
                ->get();


            $class_schedule = ClassSchedule::all()->where('schedule_id', $id);

            $rooms  = Rooms::all()->sortByDesc('id');


            $enrolled_students = DB::table('students')
                ->join('enrollments', 'students.id', '=', 'enrollments.student_id')
                ->where('enrollments.course_code', $schedule->schedule_course_id)
                ->get();

            $schedule_history = ScheduleLogs::all()->take(20)->where('course_code', $schedule->schedule_course_id);

            $schedule_list = Schedule::all();

            $assessors  = DB::table('instructor_courses')
                ->join('instructors', 'instructors.id', '=', 'instructor_courses.instructor_id')
                ->where('instructor_courses.position', '=', 'Assessor')
                ->whereNull('instructor_courses.deleted_at')
                ->whereNull('instructors.deleted_at')
                // ->groupBy('instructors.id') //add a line
                ->get();

            $supervisors  = DB::table('instructor_courses')
                ->join('instructors', 'instructors.id', '=', 'instructor_courses.instructor_id')
                ->where('instructor_courses.position', '=', 'Supervisor')
                ->whereNull('instructor_courses.deleted_at')
                ->whereNull('instructors.deleted_at')
                // ->groupBy('instructors.id') //add a line
                ->get();

            $subcourses = DB::table('subcourses')
                ->where('course_id', $schedule->course_id)
                ->get();


            return view('Schedule.schedule-information', compact('schedule', 'assessors', 'supervisors', 'class_schedule', 'rooms', 'instructors', 'enrolled_students', 'schedule_history', 'schedule_list',
                'subcourses'));
        // }
        // catch (\Exception $e){
        //     $notification = array(
        //         'message' => 'Fatal Error, Please Contact Administrator',
        //         'alert-type' => 'error'
        //     );

        //     $this->generalUtil->errorLogs("Schedule", $e->getCode(), $e->getMessage());
        //     return redirect()->back()->with($notification);
        // }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function edit(Schedule $schedule)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        try{
            Schedule::find($id)->update($request->all());

            $notification = array(
                'message' => 'Schedule Updated Successfully',
                'alert-type' => 'info'
            );

            return redirect()->back()->with($notification);
        }
        catch (\Exception $e){
            $notification = array(
                'message' => 'Fatal Error, Please Contact Administrator',
                'alert-type' => 'error'
            );

            $this->generalUtil->errorLogs("Schedule", $e->getCode(), $e->getMessage());
            return redirect()->back()->with($notification);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function destroy(Schedule $schedule)
    {
        //
    }

    public function getSupervisor($course_id)
    {
        $supervisor = DB::table('instructor_courses')
            ->join('instructors', 'instructors.id', '=', 'instructor_courses.instructor_id')
            ->where('instructor_courses.position', '=', 'Supervisor')
            ->where('course_id', $course_id)
            ->whereNull('instructor_courses.deleted_at')
            ->whereNull('instructors.deleted_at')
            ->get();

        if(!empty($supervisor)){
            foreach ($supervisor as $row){

                $resp[] = array(
                    'value'=>$row->id,
                    "name"=>utf8_decode($row->firstname . " " . $row->lastname),
                );

            }
            return response()->json($resp);
        }
        $result[] = array(
            'name'=> "No record"
        );
        return response()->json($result);

    }

    public function getAssessor($course_id)
    {
        $assessor = DB::table('instructor_courses')
            ->join('instructors', 'instructors.id', '=', 'instructor_courses.instructor_id')
            ->where('instructor_courses.position', '=', 'Assessor')
            ->where('course_id', $course_id)
            ->whereNull('instructor_courses.deleted_at')
            ->whereNull('instructors.deleted_at')
            ->get();

        if(!empty($assessor)){
            foreach ($assessor as $row){

                $resp[] = array(
                    'value'=>$row->id,
                    "name"=>utf8_decode($row->firstname . " " . $row->lastname),
                );

            }
            return response()->json($resp);
        }
        $result[] = array(
            'name'=> "No record"
        );
        return response()->json($result);

    }


    public function getInstructorList($course_id)
    {
        $instructor = DB::table('instructor_courses')
            ->join('instructors', 'instructors.id', '=', 'instructor_courses.instructor_id')
            ->where('instructor_courses.position', '=', 'Instructor')
            ->where('course_id', $course_id)
            ->whereNull('instructor_courses.deleted_at')
            ->whereNull('instructors.deleted_at')
            ->get();

        if(!empty($instructor)){
            foreach ($instructor as $row){

                $resp[] = array(
                    'value'=>$row->id,
                    "name"=>utf8_decode($row->firstname . " " . $row->lastname),
                );

            }
            return response()->json($resp);
        }
        $result[] = array(
            'name'=> "No record"
        );
        return response()->json($result);

    }

    private function getCourseById($course_id){
        $query = Courses::findOrFail($course_id);

        return $query;

    }

    private function generateCourseID($course_id){

        $schedule_count = Schedule::all()->where('course_id', $course_id)->count() + 1;
        $course = Courses::findOrFail($course_id);
        $course_code = $course->course_code;
        $count = sprintf("%04d", $schedule_count);

        $generated_code = "NOTIP-".$course_code."-".$count;

        return $generated_code;


    }

    public static function getRoom($id){

        $room = Rooms::findOrFail($id);

        return $room->room_name;

    }

    public static function getInstructor($id){
        $instructor = Instructor::find($id);


        if (isset($instructor)){
            return $instructor->firstname . " " . $instructor->lastname;
        }

        return "NONE";
    }

    public static function getRank($id){
        $rank = Ranks::find($id);

        if (isset($rank)){
            return $rank->rank;
        }

        return null;

    }
}
