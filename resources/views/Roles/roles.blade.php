@extends('layout.app')

@section('content')
    <main class="main">
        <!-- Breadcrumb -->

        <div class="container-fluid">

            <div class="animated fadeIn">

                <div class="row mt-lg-5">
                    <div class="col-md-12">
                        <div class="card card-accent-theme">
                            <div class="card-body">
                                @can('roles-create')
                                <div align="right" >
                                    <a href="{{ url('roles/create') }}" class="btn btn-primary">Create </a>
                                </div>
                                @endcan
                                <h4 class="text-theme">
                                    Roles
                                </h4>
                                <table class="table table-hover dataTable table-striped w-full" data-plugin="dataTable" width="100%">
                                    <thead>
                                    <tr>
                                        <th>Role Name</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @foreach($data as $row)
                                        <tr>
                                            <td>{{ $row->name }}</td>
                                            <td>
                                                @can('roles-edit')
                                                    <a href="{{ url('roles/'.$row->id.'/edit') }}" class="btn
                                                            btn-xs
                                                            btn-warning" style="margin-top: -5px"title="Edit Record"><i class="icon-pencil"
                                                                                                                        style="color: black;"></i>
                                                    </a>
                                                @endcan
                                                @can('roles-delete')
                                                    <a href="" data-target="#delete{{ $row->id }}" data-toggle="modal" class="btn
                                                                btn-xs  btn-danger" style="margin-top: -5px" title="Delete Record"><i class="icon-trash"
                                                                                                                                      style="color: white;"></i>
                                                    </a>
                                                @endcan
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                            </div>
                            <!-- end card-body -->
                        </div>
                        <!-- end card -->
                    </div>
                </div>
            </div>
            <!-- end animated fadeIn -->
        </div>
        <!-- end container-fluid -->


    </main>

    @foreach($data as $row)
        <div id="delete{{ $row->id }}" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h6 class="modal-title">Remove Record</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <form action="/roles/{{ $row->id }}" method="POST" id="needs-validation" novalidate="" enctype="multipart/form-data">
                        @csrf
                        @method('DELETE')

                        <div class="modal-body">
                            <h5>Delete This Record?</h5>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-danger">Remove</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    @endforeach


@endsection
@section('script')
    <script>
        console.log('employee users')
    </script>
@endsection
