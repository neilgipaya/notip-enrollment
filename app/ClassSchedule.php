<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClassSchedule extends Model
{
    //

    protected $table = "class_schedules";

    protected $guarded = [];

}
