<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $permissions = [
            'admin-dashboard',
            'user-profile',
            'change-password',
            'students-list',
            'enroll-students',
            'reserve-students',
            'view-student-info',
            'edit-students',
            'delete-students',
            'schedules-list',
            'view-schedule-info',
            'create-schedules',
            'delete-schedules',
            'schedules-history',
            'schedule-edit',
            'schedule-delete',
            'grades-encoding',
            'certificate-listing',
            'certificate-printing',
            'releasing',
            'payment-list',
            'pending-payments-list',
            'downpayments-list',
            'waiting-list',
            'payments-manage',
            'configuration-access',
            'user-list',
            'user-create',
            'user-edit',
            'user-delete',
            'roles-list',
            'roles-create',
            'roles-edit',
        ];

        foreach ($permissions as $permission) {
            Permission::create(['name' => $permission]);
        }

    }
}
