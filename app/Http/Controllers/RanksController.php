<?php

namespace App\Http\Controllers;

use App\Ranks;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class RanksController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        return view('Settings.ranks', [
            'data'=>Ranks::all()->sortByDesc('id')
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
        Ranks::create($request->all());
        $notification = array(
            'message' => 'Rank Created Successfully',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param Ranks $Ranks
     * @return void
     */
    public function show(Ranks $Ranks)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Ranks $Ranks
     * @return Response
     */
    public function edit(Ranks $Ranks)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
        Ranks::find($id)->update($request->all());
        $notification = array(
            'message' => 'Rank Updated Successfully',
            'alert-type' => 'info'
        );
        return redirect()->back()->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return Response
     */
    public function destroy($id)
    {
        //

        Ranks::find($id)->delete();
        $notification = array(
            'message' => 'Rank Deleted Successfully',
            'alert-type' => 'error'
        );
        return redirect()->back()->with($notification);
    }
}
