@extends('layout.app')

@section('content')
    @inject('instructor', 'App\Http\Controllers\InstructorController')
    @inject('classSchedule', 'App\Http\Controllers\ScheduleController')
    @inject('courseController', 'App\Http\Controllers\CoursesController')

    <main class="main">
        <!-- Breadcrumb -->

        <div class="container-fluid">

            <div class="animated fadeIn">
                <div class="row mt-lg-5">
                    <div class="col-md-3">
                        <div class="card card-accent-theme">
                            <div class="card-header">
                                <h6 class="text-theme">Schedule Information</h6>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h6>{{ $schedule->course }}</h6>
                                    </div>
                                </div>
                                <table class="table table-striped m-md-b-0">
                                    <tbody>
                                    <tr>
                                        <th scope="row">Sub Course</th>
                                        <td class="text-right">{{ $courseController::subCourse($schedule->schedule_sub_course_id) }}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Duration</th>
                                        <td class="text-right">{{ $schedule->course_duration }}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Slots</th>
                                        <td class="text-right">{{ $schedule->slots }}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Assessor</th>
                                        <td class="text-right">{{ $instructor::getAssessor($schedule->schedule_assessor_id) }}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Supervisor</th>
                                        <td class="text-right">{{ $instructor::getSuperVisor($schedule->schedule_supervisor_id)}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- end card-body -->
                        </div>
                        <!-- end card -->
                    </div>
                    <div class="col-md-9">
                        <div class="card card-accent-theme">
                            <div class="card-body">
                                <h4 class="text-theme">Schedule Information</h4>
                                <ul class="nav nav-tabs custom-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#enrolled" role="tab" aria-controls="info" aria-selected="true">
                                            <i class="fa fa-user-circle-o"></i> Enrolled Students</a>
                                    </li>
                                </ul>

                                <div class="tab-content">
                                    <div class="tab-pane active" id="enrolled" role="tabpanel">
                                        <div class="row">
                                            <table class="display table table-hover table-striped dataTable" data-plugin="datatable" style="width: 100%">
                                                <thead>
                                                <tr>
                                                    <th>Profile</th>
                                                    <th>SRN</th>
                                                    <th>Full Name</th>
                                                    <th>Rank</th>
                                                    <th>Grade</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($enrolled_students as $row)
                                                    <tr>
                                                        <td><img src="{{ $row->profile }}" class="img img-avatar" style="width: 100px" /> </td>
                                                        <td>{{ $row->srn }}</td>
                                                        <td>{{ $row->firstname . " " . $row->lastname  }}</td>
                                                        <td>{{ $classSchedule::getRank($row->rank_id)}}</td>
                                                        <td>
                                                            {{ $row->grade }}
                                                        </td>
                                                        <td>
                                                            <a class="btn btn-info btn-sm m-0" title="Print Certificate" data-toggle="modal" data-target="#print{{ $row->id }}">
                                                                <i class="fa fa-print"></i>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- end-card-body -->
                        </div>
                        <!-- end card -->
                    </div>
                </div>
            </div>
            <!-- end animated fadeIn -->
        </div>
        <!-- end container-fluid -->
    </main>


    <!-- modal includes -->

    @foreach($enrolled_students as $row)
        <div id="print{{ $row->id }}" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h6 class="modal-title">Print Certificate</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <form action="/certificatePrinting" method="POST" id="needs-validation" novalidate="" enctype="multipart/form-data">
                        @csrf

                        <input type="hidden" class="form-control" name="student_id" value="{{ $row->student_id }}" />
                        <input type="hidden" class="form-control" name="enrollment_course_id" value="{{ $row->course_code }}" />
                        <input type="hidden" value="{{ $row->registration_no }}" name="registration_number" />
                        <div class="modal-body">
{{--                            <div class="form-group">--}}
{{--                                <label>Registration Number</label>--}}
{{--                                <input type="text" class="form-control" name="registration_number" required />--}}
{{--                            </div>--}}
                            <div class="form-group">
                                <label>Select Format</label>
                                <select class="form-control select2" data-plugin="select2" name="format" style="width: 100%">
                                    @foreach($format as $row)
                                        <option value="{{ $row->id }}">{{ $row->certificate_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-danger">Print</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    @endforeach

@endsection

@section('script')


@endsection
