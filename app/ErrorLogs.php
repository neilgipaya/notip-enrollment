<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ErrorLogs extends Model
{
    //

    protected $table = "error_logs";

    protected $guarded = [];
}
