<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Mockery\Exception;
use Spatie\Permission\Models\Role;

class UsersController extends Controller
{
    /**
     * @var GeneralUtil
     */
    private $generalUtil;

    /**
     * UsersController constructor.
     * @param GeneralUtil $generalUtil
     */
    public function __construct(GeneralUtil $generalUtil)
    {
        $this->generalUtil = $generalUtil;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = User::where('role', '!=', 1)->get();
        $roles = Role::where('name', '!=', 'Super Admin')->get();


        return view('Users.user-list', compact('data', 'roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        $roles = Role::where('name', '!=', 'Super Admin')->get();

        return view('Users.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try{

            $user = User::create($request->all());
            $user->assignRole($request->post('role'));

            $notification = array(
                'message' => 'User Created Successfully',
                'alert-type' => 'success'
            );

            return redirect()->back()->with($notification);

        } catch (Exception $e){

            $notification = array(
                'message' => 'Fatal Error, Please Contact Administrator',
                'alert-type' => 'error'
            );

            $this->generalUtil->errorLogs("Users", $e->getCode(), $e->getMessage());
            return redirect()->back()->with($notification);
        }



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        try{

            $user = User::find($id)->update($request->all());
            $roles = $request->post('role');
            if (isset($roles)){
                $user->syncRoles($request->post('role'));

            }

            $notification = array(
                'message' => 'User Updated Successfully',
                'alert-type' => 'info'
            );

            return redirect()->back()->with($notification);
        } catch (Exception $e){
            $notification = array(
                'message' => 'Fatal Error, Please Contact Administrator',
                'alert-type' => 'error'
            );

            $this->generalUtil->errorLogs("Users", $e->getCode(), $e->getMessage());
            return redirect()->back()->with($notification);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try{
            User::find($id)->detele();
            $notification = array(
                'message' => 'User Deleted Successfully',
                'alert-type' => 'warning'
            );

            return redirect()->back()->with($notification);
        } catch (Exception $e){
            $notification = array(
                'message' => 'Fatal Error, Please Contact Administrator',
                'alert-type' => 'error'
            );

            $this->generalUtil->errorLogs("Users", $e->getCode(), $e->getMessage());
            return redirect()->back()->with($notification);
        }
    }
}
