<?php

namespace App\Http\Controllers;

use App\Courses;
use App\Enrollment;
use App\Payables;
use App\Schedule;
use App\Students;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    /**
     * @var GeneralUtil
     */
    private $generalUtil;

    /**
     * DashboardController constructor.
     * @param GeneralUtil $generalUtil
     */
    public function __construct(GeneralUtil $generalUtil)
    {
        $this->generalUtil = $generalUtil;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $data['totalEmployee'] = User::all()->count();
        $data['totalCourses'] = Courses::all()->count();
        $data['totalStudents'] = Students::all()->count();
        $data['totalSchedules'] = Schedule::all()->count();
        $data['reservedStudents'] = Students::whereNotNull('reservation_no')->count();
        $data['enrolledStudents'] = Enrollment::all()->count();
        $data['pendingPayment'] = Payables::all()->where('status', 'Pending')->count();
        $data['downpayments'] = Payables::all()->where('status', 'Downpayments')->count();
        $data['expiration'] = DB::table('instructors')
            ->join('instructor_courses', 'instructors.id', '=', 'instructor_courses.instructor_id')
            ->get();

        $query = [];
        $expiry = [];
        foreach ($data['expiration'] as $row){
            $query['name'] = $row->firstname . " " . $row->lastname;
            $query['course'] = $this->generalUtil->getCourseById($row->course_id)->course;
            $query['accreditation_expiration'] = $row->accreditation_expiration;
            if (Carbon::parse($query['accreditation_expiration'])->format('Y-m') < Carbon::today()->format('Y-m')){
                $query['date_difference'] = $this->generalUtil->diffInMonths([
                    $query['accreditation_expiration'],
                    Carbon::today()
                ]);
            } else {
                $query['date_difference'] = 0;
            }

            if ($query['date_difference'] >= 6){
                $expiry[] = $query;
            }
        }

        $data['expiring_accreditation'] = $expiry;

        return view('Dashboard.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
