<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Instructor extends Model
{
    //

    protected $guarded = [];

    public function instructor_courses(){
        return $this->belongsTo(InstructorCourses::class);
    }
}
