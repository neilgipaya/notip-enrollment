<table class="display table table-hover table-striped dataTable" data-plugin="datatable" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th>Date</th>
        <th>Schedule Type</th>
        <th>Sub Course</th>
        <th>Time From </th>
        <th>Time To</th>
        <th>Instructor </th>
        <th>Room</th>
        <th>Duration</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    @foreach($class_schedule as $row)
        @if(\Carbon\Carbon::parse($row->date)->dayOfWeek == \Carbon\Carbon::SUNDAY)
            <tr>
                <td colspan="8">
                    <center>SUNDAY</center>
                </td>
            </tr>
        @else
            <tr>
                <td>{{ $row->date }}</td>
                <td>
                    {{ $row->schedule_type }}
                </td>
                <td>
                    {{ $courseController::subCourse($row->schedule_course_id) }}
                </td>
                <td>
                    @if(isset($row->time_from))
                        {{ \Carbon\Carbon::parse($row->time_from)->format('g:i A') }}
                    @endif
                </td>
                <td>
                    @if(isset($row->time_to))
                        {{ \Carbon\Carbon::parse($row->time_to)->format('g:i A') }}
                    @endif
                </td>
                <td>
                    @if(isset($row->instructor))
                        {{ $classSchedule::getInstructor($row->instructor) }}
                    @endif
                </td>
                <td>
                    @if(isset($row->room_id))
                        {{ $classSchedule::getRoom($row->room_id) }}
                    @endif
                </td>
                <td>
                    {{ $row->duration }}
                </td>
                <td class="text-nowrap" style="margin: 0">
                    <a class="btn btn-warning btn-sm m-0" href="#" data-toggle="modal" data-target="#edit{{ $row->id }}">
                        <i class="fa fa-pencil"></i>
                    </a>
                </td>
            </tr>

        @endif
    @endforeach
    </tbody>
</table>

